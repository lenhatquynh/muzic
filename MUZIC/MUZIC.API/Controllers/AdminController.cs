﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using MUZIC.Model;
using System.Data;

namespace MUZIC.API.Controllers
{
    public class AdminController : ConBase
    {
        [HttpGet("get/songs_cnt/{word?}")]

        public async Task<ActionResult<object>> GetListSongCnt(string? word)
        {
            int cnt = await Program.Sql.ExecuteScalarAsync<int>(@"
                Select count(*) as cnt from Song
                where dbo.rmvAccent(Title) like concat(N'%',dbo.rmvAccent(@word),'%')
                or dbo.rmvAccent(Artist) like concat(N'%',dbo.rmvAccent(@word),'%')
                or dbo.rmvAccent(Genre) like concat(N'%',dbo.rmvAccent(@word),'%')
            ", new { word });
            return new
            {
                status = 0,
                data = cnt
            };
        }
        [HttpGet("get/users_cnt/{word?}")]
        public async Task<ActionResult<object>> GetListUserCnt(string? word)
        {
            int cnt = await Program.Sql.ExecuteScalarAsync<int>(@"
                Select count(*) as cnt from Account
                where dbo.rmvAccent(DisplayName) like concat(N'%',dbo.rmvAccent(@word),'%')
                or dbo.rmvAccent(Username) like concat(N'%',dbo.rmvAccent(@word),'%')
            ", new { word });
            return new
            {
                status = 0,
                data = cnt
            };
        }

        [HttpGet("get/feedbacks_cnt/{word?}")]
        public async Task<ActionResult<object>> GetListFeedbackCnt(string? word)
        {
            int cnt = await Program.Sql.ExecuteScalarAsync<int>(@"
                Select count(*) as cnt from Feedback
                where dbo.rmvAccent(Title) like concat(N'%',dbo.rmvAccent(@word),'%')
            ", new { word });
            return new
            {
                status = 0,
                data = cnt
            };
        }

        [HttpGet("get/songs/{page_id}/{item_per_page}/{word?}")]
        public async Task<ActionResult<object>> GetListSong(int page_id, int item_per_page, string? word)
        {
            if (word is null) word = "";
            var li = (await Program.Sql.QueryAsync<Song>(@"
                declare @_page_id as int
                set @_page_id = @page_id
                declare @_item_per_page as int
                set @_item_per_page = @item_per_page
                Select * from Song 
                where dbo.rmvAccent(Title) like concat(N'%',dbo.rmvAccent(@word),'%')
                or dbo.rmvAccent(Artist) like concat(N'%',dbo.rmvAccent(@word),'%')
                or dbo.rmvAccent(Genre) like concat(N'%',dbo.rmvAccent(@word),'%')
                order by Title asc
                OFFSET (@_page_id-1)*@_item_per_page ROWS FETCH NEXT @_item_per_page  ROWS ONLY
            ", new { page_id, item_per_page, word })).AsList();
            return new
            {
                status = 0,
                data = li,
            };
        }
        [HttpGet("get/users/{page_id}/{item_per_page}/{word?}")]
        public async Task<ActionResult<object>> GetListUser(int page_id, int item_per_page, string? word)
        {
            if (word is null) word = "";
            var li = (await Program.Sql.QueryAsync<Account>(@"
                declare @_page_id as int
                set @_page_id = @page_id
                declare @_item_per_page as int
                set @_item_per_page = @item_per_page
                Select * from Account 
                where dbo.rmvAccent(DisplayName) like concat(N'%',dbo.rmvAccent(@word),'%')
                or dbo.rmvAccent(Username) like concat(N'%',dbo.rmvAccent(@word),'%')
                order by DisplayName asc
                OFFSET (@_page_id-1)*@_item_per_page ROWS FETCH NEXT @_item_per_page  ROWS ONLY
            ", new { page_id, item_per_page, word })).AsList();
            return new
            {
                status = 0,
                data = li,
            };
        }

        [HttpGet("get/feedbacks/{page_id}/{item_per_page}/{word?}")]
        public async Task<ActionResult<object>> GetListFeedback(int page_id, int item_per_page, string? word)
        {
            if (word is null) word = "";
            var li = (await Program.Sql.QueryAsync<Feedback>(@"
                declare @_page_id as int
                set @_page_id = @page_id
                declare @_item_per_page as int
                set @_item_per_page = @item_per_page
                Select * from Feedback 
                where dbo.rmvAccent(Title) like concat(N'%',dbo.rmvAccent(@word),'%')
                order by Title asc
                OFFSET (@_page_id-1)*@_item_per_page ROWS FETCH NEXT @_item_per_page  ROWS ONLY
            ", new { page_id, item_per_page, word })).AsList();
            return new
            {
                status = 0,
                data = li,
            };
        }

        [HttpPost("update/feedbacks")]
        public async Task<ActionResult<object>> UpdateFeedbackPriority([FromForm] int id, [FromForm] int priority)
        {
            await Program.Sql.ExecuteAsync("update Feedback set Priority = @priority where Id = @id", new { priority, id });
            return new
            {
                status = 0
            };
        }


        [HttpPost("update/accounts")]
        public async Task<ActionResult<object>> UpdateAccountPriority([FromForm] string Username, [FromForm] int Status)
        {
            await Program.Sql.ExecuteAsync("update Account set Status = @Status where Username = @Username", new { Username, Status });
            return new
            {
                status = 0
            };
        }

        [HttpPost("update/songs")]
        public async Task<ActionResult<object>> UpdateSongStatus([FromForm] int Id, [FromForm] int Status)
        {
            await Program.Sql.ExecuteAsync("update Song set Status = @Status where Id = @Id", new { Id, Status });
            return new
            {
                status = 0
            };
        }



        [HttpGet("get/dashboardmetric")]
        public async Task<ActionResult<object>> GetDashboardMetric()
        {
            var metric = (await Program.Sql.QueryAsync<DashboardMetric>(@"
                select
	                (select count(*) from Feedback) as cnt_feedback,
	                (select count(*) from Account) as cnt_account,
	                (select count(*) from Song) as cnt_song,
	                (select count(*) from Playlist) as cnt_playlist,
	                (select count(*) from WebTracking where cast(Date as Date) = cast(CURRENT_TIMESTAMP as Date)) as today_web_visited,
	                (select count(*) from SongTracking where cast(Date as Date) = cast(CURRENT_TIMESTAMP as Date) and EventType=1) as today_song_view,
	                (select count(*) from Song where cast(Date as Date) = cast(CURRENT_TIMESTAMP as Date)) as today_song_created,
	                (select count(*) from Playlist where cast(Date as Date) = cast(CURRENT_TIMESTAMP as Date)) as today_playlist_created,
	                (select count(*) from Account where cast(Date as Date) = cast(CURRENT_TIMESTAMP as Date)) as today_account_created,
	                (select count(*) from Feedback where cast(Date as Date) = cast(CURRENT_TIMESTAMP as Date)) as today_feedback_view,
	                (select count(*) from SongTracking  where EventType=1)  as total_song_view,
	                (select count(*) from PlaylistTracking where EventType=1)  as total_playlist_view,
	                (select count(*) from SongTracking where EventType=2) +
	                (select count(*) from PlaylistTracking where EventType=2)  as total_favorite,
	                (select count(distinct Genre) from Song) as total_genres
            ")).First();
            return new
            {
                status = 0,
                data = metric
            };
        }

        [HttpGet("get/dashboardfeedbacks")]
        public async Task<ActionResult<object>> GetDashboardFeedback()
        {
            var li = (await Program.Sql.QueryAsync<string>("select top(3) Title from Feedback order by Date desc")).AsList();
            return new
            {
                status = 0,
                data = li,
            };
        }

        [HttpGet("get/dashboardtrafic/{Date}")]
        public async Task<ActionResult<object>> GetDashboardTrafic(string Date)
        {
            var li = (await Program.Sql.QueryAsync<int>(@"
            with base as (
              select 0  as gr
              union select 1
              union select 2
              union select 3
              union select 4
              union select 5
              union select 6
              union select 7
              union select 8
              union select 9
              union select 10
              union select 11
            ), hour_event_today as (
              select DATEPART(HOUR, date)/2 as gr from WebTracking
              where cast(date as date) = cast(@Date as date)
            ), hourly_count as (
              select gr, count(*) as cnt from hour_event_today
              group by gr
            ) 
            select 
              case
                when cnt is null then 0
                else cnt
              end as cnt
            from base as b left join hourly_count as h on b.gr = h.gr
            ", new { Date })).AsList();
            return new
            {
                status = 0,
                data = li,
            };
        }

        [HttpPost("insert/webtracking")]
        public async Task<ActionResult<object>> InsertWebtracking([FromForm] string? Username, [FromForm] string ContextURL)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("WebTracking", "Username", "ContextURL"), new { Username, ContextURL });
            return new
            {
                status = 0
            };
        }

        [HttpGet("get/useruploadinfo/{Username}")]
        public async Task<ActionResult<object>> GetUseruploadinfo(string Username)
        {
            //cnt_song, last_date
            var data = (await Program.Sql.ExecuteReaderAsync(@"
                select 
	                count(*) as cnt_song,
	                case
                        when count(*)=0 then '13.05.2000'
                        else FORMAT(max(Date), 'dd.MM.yyyy')
                    end as last_date
                from Song where Username = @Username
            ", new { Username }));
            DataTable dt = new DataTable();
            dt.Load(data);
            return new
            {
                status = 0,
                data = new
                {
                    cnt_song = dt.Rows[0]["cnt_song"],
                    last_date = dt.Rows[0]["last_date"]
                }
            };
        }
    }
}

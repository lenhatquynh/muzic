﻿using Microsoft.AspNetCore.Mvc;

namespace MUZIC.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public abstract class ConBase : ControllerBase
    {
    }
}

﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using MUZIC.Model;
using System.Text.Json;

namespace MUZIC.API.Controllers
{
    public class FeedbackController : ConBase
    {
        [HttpPost("send")]
        public async Task<ActionResult<object>> Send(JsonElement json)
        {
            var feedback = json.GetObject<Feedback>(true);
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("Feedback", "Username", "Title", "Detail"), feedback);
            return new
            {
                status = 0
            };
        }
    }
}

﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using MUZIC.Model;

namespace MUZIC.API.Controllers
{
    public class TrackingController : ConBase
    {
        [HttpPost("add/viewcount/song")]
        public async Task<ActionResult<object>> AddSongViewCount([FromForm] string username, [FromForm] int songId)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("SongTracking", "Username", "SongId", "EventType"), new
            {
                Username = username,
                SongId = songId,
                EventType = 1
            });
            return new
            {
                status = 0
            };
        }

        [HttpPost("add/viewcount/playlist")]
        public async Task<ActionResult<object>> AddPlaylistViewCount([FromForm] string username, [FromForm] int playlistId)
        {
            await Program.Sql.ExecuteAsync(Extension.GetInsertQuery("PlaylistTracking", "Username", "PlaylistId", "EventType"), new
            {
                Username = username,
                PlaylistId = playlistId,
                EventType = 1
            });
            return new
            {
                status = 0
            };
        }

        [HttpGet("get/topview/song/{day}/{top:int}")]
        public async Task<ActionResult<object>> GetTopViewSong(string day, int top)
        {
            var res = await Program.Sql.QueryAsync<TopSong>(@"
                with public_song_id as (
                    select Id from Song where IsPublic = 1 and Status = 0
                ), popular_song as (
                    select top(@Top) SongId, count(*) as cnt from SongTracking
                    where Date >= @Day and Date < dateadd(day, 1, @Day)
                    and EventType = 1
                    and SongId in (select Id from public_song_id)
                    group by SongId
                    order by cnt desc
                ) select Song.*, popular_song.cnt from Song  right join popular_song on Song.Id = popular_song.SongId",
            new
            {
                Day = day,
                Top = top
            });
            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/topview/all/song/{top:int}")]
        public async Task<ActionResult<object>> GetTopViewSongAllTime(int top)
        {
            var res = await Program.Sql.QueryAsync<TopSong>(@"
                with public_song_id as (
                    select Id from Song where IsPublic = 1 and Status = 0
                ), popular_song as (
                    select top(@Top) SongId, count(*) as cnt from SongTracking
                    where EventType = 1
                    and SongId in (select Id from public_song_id)
                    group by SongId
                    order by cnt desc
                ) select Song.*, popular_song.cnt from Song  right join popular_song on Song.Id = popular_song.SongId",
            new
            {
                Top = top
            });
            return new
            {
                status = 0,
                data = res
            };
        }


        [HttpGet("get/topview/playlist/{day}/{top:int}")]
        public async Task<ActionResult<object>> GetTopViewPlaylist(string day, int top)
        {
            var res = await Program.Sql.QueryAsync<TopPlaylist>(@"
                with public_playlist_id as (
                  select Id from Playlist where IsPublic = 1
                ), popular_playlist as (
                  select top(@Top) PlaylistId, count(*) as cnt from PlaylistTracking
                  where Date >= @Day and Date < dateadd(day, 1, @Day)
                  and EventType = 1
                  and PlaylistId in (select Id from public_playlist_id)
                  group by PlaylistId
                  order by cnt desc
                ) select Playlist.*, popular_playlist.cnt from Playlist right join popular_playlist on Playlist.Id = popular_playlist.SongId",
            new
            {
                Day = day,
                Top = top
            });

            return new
            {
                status = 0,
                data = res
            };
        }


        [HttpGet("get/topview/all/playlist/{top:int}")]
        public async Task<ActionResult<object>> GetTopViewPlaylistAllTime(int top)
        {
            var res = await Program.Sql.QueryAsync<TopPlaylist>(@"
                with public_playlist_id as (
                  select Id from Playlist where IsPublic = 1
                ), popular_playlist as (
                  select top(@Top) PlaylistId, count(*) as cnt from PlaylistTracking
                  where EventType = 1
                  and PlaylistId in (select Id from public_playlist_id)
                  group by PlaylistId
                  order by cnt desc
                ) select Playlist.*, popular_playlist.cnt from Playlist right join popular_playlist on Playlist.Id = popular_playlist.PlaylistId",
            new
            {
                Top = top
            });

            return new
            {
                status = 0,
                data = res
            };
        }


        [HttpGet("get/topfavorite/song/{day}/{top:int}")]
        public async Task<ActionResult<object>> GetTopFavoriteSong(string day, int top)
        {
            var res = await Program.Sql.QueryAsync<TopSong>(@"
                with public_song_id as (
                  select Id from Song where IsPublic = 1 and Status = 0
                ), with_row_num as (
                  select 
                    Username, 
                  SongId,
                  EventType,
                    ROW_NUMBER() over(partition by Username, SongId order by Date desc) as row_num
                  from SongTracking
                  where (EventType = 2 or EventType = 3)
                  and Date >= @Day and Date < dateadd(day, 1, @Day)
                  and SongId in (select Id from public_song_id)
                ), favo as (
                  select SongId from with_row_num where row_num = 1  and EventType=2
                ), favo_song as (
                  select top(@Top) SongId, count(*) as cnt from favo
                  group by SongId
                  order by cnt desc
                ) select Song.*, favo_song.cnt from Song  right join favo_song on Song.Id = favo_song.SongId",
            new
            {
                Day = day,
                Top = top
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/topfavorite/all/song/{top:int}")]
        public async Task<ActionResult<object>> GetTopFavoriteSongAllTime(int top)
        {
            var res = await Program.Sql.QueryAsync<TopSong>(@"
                with public_song_id as (
                  select Id from Song where IsPublic = 1 and Status = 0
                ), with_row_num as (
                  select 
                    Username, 
                  SongId,
                  EventType,
                    ROW_NUMBER() over(partition by Username, SongId order by Date desc) as row_num
                  from SongTracking
                  where (EventType = 2 or EventType = 3)
                  and SongId in (select Id from public_song_id)
                ), favo as (
                  select SongId from with_row_num where row_num = 1  and EventType=2
                ), favo_song as (
                  select top(@Top) SongId, count(*) as cnt from favo
                  group by SongId
                  order by cnt desc
                ) select Song.*, favo_song.cnt from Song  right join favo_song on Song.Id = favo_song.SongId",
            new
            {
                Top = top
            });

            return new
            {
                status = 0,
                data = res
            };
        }


        [HttpGet("get/topfavorite/playlist/{day}/{top:int}")]
        public async Task<ActionResult<object>> GetTopFavoritePlaylist(string day, int top)
        {
            var res = await Program.Sql.QueryAsync<Playlist>(@"
                with public_playlist_id as (
                  select Id from Playlist where IsPublic = 1
                ), with_row_num as (
                  select 
                    Username, 
                  PlaylistId,
                  EventType,
                    ROW_NUMBER() over(partition by Username, PlaylistId order by Date desc) as row_num
                  from PlaylistTracking
                  where (EventType = 2 or EventType = 3)
                  and Date >= @Day and Date < dateadd(day, 1, @Day)
                  and PlaylistId in (select Id from public_playlist_id)
                ), favo as (
                  select PlaylistId from with_row_num where row_num = 1  and EventType=2
                ), favo_playlist as (
                  select top(@Top) PlaylistId, count(*) as cnt from favo
                  group by PlaylistId
                  order by cnt desc
                ) select Playlist.*, favo_playlist.cnt from Playlist right join favo_playlist on Playlist.Id = favo_playlist.PlaylistId",

            new
            {
                Day = day,
                Top = top
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/topfavorite/all/playlist/{top:int}")]
        public async Task<ActionResult<object>> GetTopFavoritePlaylistAllTime(int top)
        {
            var res = await Program.Sql.QueryAsync<TopPlaylist>(@"
                with public_playlist_id as (
                  select Id from Playlist where IsPublic = 1
                ), with_row_num as (
                  select 
                    Username, 
                  PlaylistId,
                  EventType,
                    ROW_NUMBER() over(partition by Username, PlaylistId order by Date desc) as row_num
                  from PlaylistTracking
                  where (EventType = 2 or EventType = 3)
                  and PlaylistId in (select Id from public_playlist_id)
                ), favo as (
                  select PlaylistId from with_row_num where row_num = 1  and EventType=2
                ), favo_playlist as (
                  select top(@Top) PlaylistId, count(*) as cnt from favo
                  group by PlaylistId
                  order by cnt desc
                ) select Playlist.*, favo_playlist.cnt from Playlist right join favo_playlist on Playlist.Id = favo_playlist.PlaylistId",
            new
            {
                Top = top
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/viewcount/song/{songId:int}")]
        public async Task<ActionResult<object>> GetSongViewCount(int songId)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select count(*) as cnt from SongTracking
                where SongId=@SongId and EventType = 1",
            new
            {
                SongId = songId
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/viewcount/playlist/{playlistId:int}")]
        public async Task<ActionResult<object>> GetPlaylistViewCount(int playlistId)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select count(*) as cnt from PlaylistTracking
                where PlaylistId=@PlaylistId and EventType = 1",
            new
            {
                playlistId = playlistId
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/yesterday/view/song/{songId:int}")]
        public async Task<ActionResult<object>> GetSongViewsYesterday(int songId)
        {
            var res = await Program.Sql.QueryAsync<int>(@"
                with base as (
                  select 0  as gr
                  union select 1
                  union select 2
                  union select 3
                  union select 4
                  union select 5
                  union select 6
                  union select 7
                  union select 8
                  union select 9
                  union select 10
                  union select 11
                ), hour_event_yesterday as (
                  select DATEPART(HOUR, date)/2 as gr from SongTracking
                  where cast(date as date) = cast(dateadd(day, -1, CURRENT_TIMESTAMP) as date) and SongId = @SongId and EventType = 1
                ), hourly_count as (
                  select gr, count(*) as cnt from hour_event_yesterday
                  group by gr
                ) 
                select
                  case
                    when cnt is null then 0
                    else cnt
                  end as cnt
                from base as b left join hourly_count as h on b.gr = h.gr",
            new
            {
                SongId = songId
            });
            return new
            {
                status = 0,
                data = res
            };
        }

        [HttpGet("get/yesterday/favorite/song/{songId:int}")]
        public async Task<ActionResult<object>> GetSongFavoritesYesterday(int songId)
        {
            var res = await Program.Sql.QueryAsync<int>(@"
                with base as (
                  select 0  as gr
                  union select 1
                  union select 2
                  union select 3
                  union select 4
                  union select 5
                  union select 6
                  union select 7
                  union select 8
                  union select 9
                  union select 10
                  union select 11
                ), hour_event_yesterday as (
                  select 
                    DATEPART(HOUR, date)/2 as gr,
                    SongId,
                    Username,
                    Date,
                    EventType
                  from SongTracking
                  where cast(date as date) = cast(dateadd(day, -1, CURRENT_TIMESTAMP) as date) 
                  and SongId = @SongId 
                  and (EventType = 2 or EventType = 3)
                ), with_row_num as (
                  select 
                    row_number() over(partition by Username, gr order by Date desc) as row_num,
                    gr,
                    EventType
                  from hour_event_yesterday
                ), hourly_count as (
                  select gr, count(*) as cnt from with_row_num
                  where row_num = 1 and EventType=2
                  group by gr
                ) 
                select  
                  case
                    when cnt is null then 0
                    else cnt
                  end as cnt
                from base as b left join hourly_count as h on b.gr = h.gr",
            new
            {
                SongId = songId
            });
            return new
            {
                status = 0,
                data = res
            };
        }

        //Quynh
        [HttpGet("get/favoritecount/song/{songId:int}")]
        public async Task<ActionResult<object>> GetSongFavoriteCount(int songId)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select count(*) as cnt from SongTracking
                where SongId=@SongId and EventType = 2",
            new
            {
                SongId = songId
            });

            return new
            {
                status = 0,
                data = res
            };
        }

        //Quynh
        [HttpGet("get/favoritecount/playlist/{playlistId:int}")]
        public async Task<ActionResult<object>> GetPlaylistFavoriteCount(int playlistId)
        {
            var res = await Program.Sql.ExecuteScalarAsync<int>(@"
                select count(*) as cnt from PlaylistTracking
                where PlaylistId=@PlaylistId and EventType = 2",
            new
            {
                playlistId = playlistId
            });

            return new
            {
                status = 0,
                data = res
            };
        }
    }
}

﻿using System.Text.Json;

namespace MUZIC.API
{
    public static class Extension
    {
        private static readonly JsonSerializerOptions serializerOptions = new()
        {
            PropertyNameCaseInsensitive = true
        };

        public static T GetObject<T>(this JsonElement json, bool caseInsensitive = false)
        {
            return caseInsensitive ? json.Deserialize<T>(serializerOptions)! : json.Deserialize<T>()!;
        }

        public static string GetInsertQuery(string table, params string[] props)
        {
            string key = string.Join(", ", props);
            string value = $"@{string.Join(", @", props)}";
            return $"insert into {table}({key}) values({value})";
        }
    }
}

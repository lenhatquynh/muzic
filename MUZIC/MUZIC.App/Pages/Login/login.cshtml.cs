using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MUZIC.App.Service;

namespace MUZIC.App.Pages.Login
{
    public class loginModel : PageModel
    {
        public async Task<IActionResult> OnGet()
        {
            var _account = await State.GetAccount(HttpContext);
            if (_account is not null) return Redirect("/profile");
            return Page();
        }

        public async Task<IActionResult> OnPost(string username, string password, string checkbox)
        {
            TempData["message"] = await AccountService.Login(username, password);
            if (TempData["message"] is not null) return Page();

            var option = new CookieOptions();
            if (checkbox is not null) option.Expires = DateTime.Now.AddDays(30);
            HttpContext.Response.Cookies.Append("username", username, option);

            return Redirect("/");
        }   
    }
}

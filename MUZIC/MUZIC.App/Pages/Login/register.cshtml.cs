using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MUZIC.App.Service;

namespace MUZIC.App.Pages.Login
{
    public class registerModel : PageModel
    {
        public IActionResult OnGet()
        {
            if (AccountService.IsLogin(HttpContext)) return Redirect("/profile");
            return Page();
        }

        public async Task<IActionResult> OnPost(string username, string email, string password, string passwordAgain)
        {
            if (password != passwordAgain)
            {
                TempData["message"] = "The two passwords does not match!";
                return Page();
            }

            TempData["message"] = await AccountService.Register(username, password, email);
            if (TempData["message"] is not null) return Page();

            TempData["message"] = "Register successfully";
            return Redirect("/login");
        }
    }
}

﻿using MUZIC.Model;
using RestSharp;
using System.Text.Json;
using MUZIC.App.Utility.Extension;

namespace MUZIC.App.Service
{
    public class AccountService
    {

        public static async Task<string?> Register(string username, string password, string email)
        {
            var request = new RestRequest("/user/create", Method.Post);
            request.AddObject(new
            {
                Username = username,
                Password = password,
                Email = email
            });

            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0) return null;
            return json.GetProperty("message").GetString();
        }
        public static async Task<string?> Login(string username, string password)
        {
            var request = new RestRequest("/user/login", Method.Post);
            request.AddObject(new
            {
                Username = username,
                Password = password
            });

            var res = await Program.Client.ExecuteAsync(request);
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                return null;

            }
            return json.GetProperty("message").GetString();
        }

        public static bool IsLogin(HttpContext h)
        {
            return h.Request.Cookies.TryGetValue("Username", out string? v);
        }

        public static async Task<Account?> Get(string Username)
        {
            var request = new RestRequest($"/user/get/{Username}", Method.Get);
            var res = await Program.Client.ExecuteAsync(request);
            if (res.ResponseStatus != ResponseStatus.Completed) return null;
            var json = JsonDocument.Parse(res.Content!).RootElement;
            if (json.GetProperty("status").GetInt16() == 0)
            {
                var r = json.GetProperty("data").GetObject<Account>();
                return r;
            }
            return null;
        }
    }
}

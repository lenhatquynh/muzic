﻿using MUZIC.Model;

namespace MUZIC.App.Service
{
    public class State
    {
        public (Account? data, bool init) _acc = (null, false);

        public HttpContext Context { get; }

        public object? this[string key]
        {
            get => Context.Items.TryGetValue(key, out object? value) ? value : null;
            set => Context.Items[key] = value;
        }

        public Account? Account {
            get {
                if (!_acc.init) _acc = ((Account?)this["_acc"], true);
                return _acc.data;
            }
        }

        public State(IHttpContextAccessor _HttpContextAccessor)
        {
            Context = _HttpContextAccessor.HttpContext!;
        }

        public static async Task<Account?> GetAccount(HttpContext Context) {
            Context.Request.Cookies.TryGetValue("Username", out string? username);
            if (username is null) return null;
            return await AccountService.Get(username);
        }
    }
}

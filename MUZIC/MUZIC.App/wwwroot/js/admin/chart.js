﻿document.addEventListener("DOMContentLoaded", () => {
    const labels = [];
    //const datapoints_yesterday = [0, 20, 20, 60, 60, 120, 140, 180, 120, 125, 105, 110, 170];
    //const datapoints_today = [0, 15, 20, 70, 50, 100, 120, 130, 140, 155, 105, 70, 30];

    const data = {
        labels: labels,
        datasets: [
            {
                label: 'Today',
                data: [],
                borderColor: '#3751FF',
                fill: false,
                tension: 0.4,
                pointStyle: 'line'

            },
            {
                label: 'Yesterday',
                data: [],
                borderColor: '#DFE0EB',
                fill: true,
                backgroundColor: '#DFE0EB44',
                tension: 0.4,
                pointStyle: 'line',

            }
        ]
    };

    let config = {
        type: 'line',
        data: data,
        options: {
            responsive: true,
            plugins: {
                title: {
                    display: true,
                    text: "Today's Trafic",
                    color: "#252733",
                    font: {
                        size: 19,
                        family: 'Mulish',
                        weight: 'bold'
                    },
                    padding: {
                        bottom: 10
                    },
                    align: 'start'
                },
                subtitle: {
                    display: true,
                    text: 'as of 19 Jan 2022, 00:00 AM',
                    color: '#9FA2B4',
                    font: {
                        size: 12,
                        family: 'Mulish',
                        weight: 'normal'
                    },
                    padding: {
                        bottom: 10
                    },
                    align: 'start'
                },
                legend: {
                    labels: {
                        usePointStyle: true,
                        color: '#9FA2B4',
                        font: {
                            size: 12,
                            family: 'Mulish',
                            weight: 'SemiBold'
                        }
                    },
                    align: 'end',
                }
            },
            interaction: {
                intersect: false,
            },
            scales: {
                x: {
                    display: true,
                    title: {
                        display: true
                    },
                    grid: {
                        display: false
                    }
                },
                y: {
                    display: true,
                    title: {
                        display: true
                    },
                    suggestedMin: 0,
                    suggestedMax: 1
                }
            }
        },
    };

    let suggestedMax = 1

    function addData(chart, label, today, yesterday) {
        chart.data.labels.push(label)
        chart.data.datasets[0].data.push(yesterday)
        chart.data.datasets[1].data.push(today)
        if (suggestedMax < today) suggestedMax = today
        if (suggestedMax < yesterday) suggestedMax = yesterday

        chart.config.options.scales.y.suggestedMax = suggestedMax * 5 / 3;
        chart.update();
    }

    window.show_chart = (today_data, yesterday_data, date) => {
        config.options.plugins.subtitle.text = date
        console.log(date)
        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );

        myChart.data.labels = []
        myChart.data.datasets[0].data = []
        myChart.data.datasets[1].data = []

        for (let i = 0; i < 12; ++i)
            setTimeout(() => {
                addData(myChart, (i*2+2).toString().padStart(2, 0), today_data[i], yesterday_data[i])
            }, 200*i)


        myChart.options.plugins.title.align = 'start';
        myChart.update();


        $(".overview-number,.number,.bot-table-row>.right-side").each((i, e) => {

            $({ countNum: 0 }).animate({ countNum: parseInt($(e).text()) }, {
                duration: 2000, // tune the speed here
                easing: 'easeOutCubic',
                step: function () {
                    // What todo on every count
                    $(e).text(Math.ceil(this.countNum));
                },
                complete: function () {

                }
            });

        })

    }
})
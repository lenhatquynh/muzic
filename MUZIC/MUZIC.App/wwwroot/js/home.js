﻿var slideIndex = 1;

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}


// SLIDESHOW 2 BEGIN HERE
var slideIndex2 = 0;
var slideIndex3 = 0;


function plusSlides2(slideNumber) {
    if (slideNumber == 1) {
        slideIndex2 = slideIndex2 == 1 ? 0 : 1;
        showSlides2(slideIndex2, slideNumber);
    } else {
        slideIndex3 = slideIndex3 == 1 ? 0 : 1;
        showSlides2(slideIndex3, slideNumber);
    }
}

function showSlides2(n, slideNumber) {
    var i;
    var slides = document.getElementsByClassName("column");
    if (slideNumber == 1) {
        if (n == 0) {
            for (i = 0; i < slides.length / 2; i++) {
                if (i < 5) {
                    slides[i].style.display = "inline-block";
                } else {
                    slides[i].style.display = "none";
                }
            }
        } else {
            for (i = 0; i < slides.length / 2; i++) {
                if (i < 5) {
                    slides[i].style.display = "none";
                } else {
                    slides[i].style.display = "inline-block";
                }
            }
        }

    } else if (slideNumber == 2) {
        if (n == 0) {
            for (i = slides.length / 2; i < slides.length; i++) {
                if (i < slides.length / 2 + 5) {
                    slides[i].style.display = "inline-block";
                } else {
                    slides[i].style.display = "none";
                }
            }
        } else {
            for (i = slides.length / 2; i < slides.length; i++) {
                if (i < slides.length / 2 + 5) {
                    slides[i].style.display = "none";
                } else {
                    slides[i].style.display = "inline-block";
                }
            }
        }
    }
}

function defaultOpenSlide() {
    var slides = document.getElementsByClassName("column");
    var i;
    for (i = 0; i < slides.length; i++) {
        if (i < 5 || (i >= 10 && i < 15)) {
            slides[i].style.display = "inline-block";
        } else {
            slides[i].style.display = "none";
        }
    }
}

// MOST POPULAR BEGIN HERE

function openTab(evt, songType) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(songType).style.display = "block";
    evt.currentTarget.className += " active";
}

function defaultClick() {
    document.getElementById("defaultClick").click();
}

//function onMouseOver(n) {
//    var columnimg = document.getElementsByClassName('column-img');
//    var columntext = document.getElementsByClassName('column-link-text');
//    var hoverstate = document.getElementsByClassName('column-hover');
//    var i;
//    for (i = 0; i < columnimg.length; i++) {
//        if (n == i) {
//            columnimg[i].style.display = "none";
//            columntext[i].style.display = "none";
//            hoverstate[i].style.display = "block";
//            break;
//        }
//    }


//}

//function onMouseOut(n) {
//    var columnimg = document.getElementsByClassName('column-img');
//    var columntext = document.getElementsByClassName('column-link-text');
//    var hoverstate = document.getElementsByClassName('column-hover');
//    var i;
//    for (i = 0; i < columnimg.length; i++) {
//        if (n == i) {
//            columnimg[i].style.display = "block";
//            columntext[i].style.display = "block";
//            hoverstate[i].style.display = "none";
//            break;
//        }
//    }
//}


var slideIndex4 = 0;
function plusSlides3(slideNumber) {
    slideIndex4 = slideIndex4 == 1 ? 0 : 1;
    showSlides3(slideIndex4);
}

function showSlides3(n) {
    var i;
    var slides = document.getElementsByClassName("column");
    if (n == 0) {
        for (i = 0; i < slides.length; i++) {
            if (i < 5) {
                slides[i].style.display = "inline-block";
            } else {
                slides[i].style.display = "none";
            }
        }
    } else {
        for (i = 0; i < slides.length; i++) {
            if (i < 5) {
                slides[i].style.display = "none";
            } else {
                slides[i].style.display = "inline-block";
            }
        }
    }


}
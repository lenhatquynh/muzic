var eyeChange = document.querySelector("#eye-change");
var pass = document.querySelector("#password");
var inputData = document.querySelectorAll(".input-data");
var checkLabel = document.querySelector(".remember-label");
//var checkBox = document.querySelector(".check");

//checkLabel.addEventListener("click", function () {
//  (checkBox.checked == true)?checkBox.checked = false:checkBox.checked = true;
//})

eyeChange.addEventListener("click", function () {
  if (eyeChange.classList.contains("fa-eye-slash")) {
    eyeChange.classList.remove("fa-eye-slash");
    eyeChange.classList.add("fa-eye");
    pass.type = "text";
  } else if (eyeChange.classList.contains("fa-eye")) {
    eyeChange.classList.remove("fa-eye");
    eyeChange.classList.add("fa-eye-slash");
    pass.type = "password";
  }
});

inputData.forEach((e)=>{
e.addEventListener("focus",function(){
    e.parentNode.style.borderColor = '#f46530';
})
e.addEventListener("blur",function(){
    e.parentNode.style.borderColor = '#c0c0c0';
})
});

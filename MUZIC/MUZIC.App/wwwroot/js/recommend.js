document.getElementById("defaultSlide").click();
function defaultOpenSlide() {
    var slides = document.getElementsByClassName("column");
    var i;
    for (i = 0; i < slides.length; i++) {
        if (i < 5 || (i >= 10 && i < 15)) {
            slides[i].style.display = "inline-block";
        } else {
            slides[i].style.display = "none";
        }
    }
}
var slideIndex2 = 0;
defaultOpenSlide(slideIndex2);
function plusSlides2(slideNumber) {
    if (slideNumber == 1) {
        slideIndex2 = slideIndex2 == 1 ? 0 : 1;
        showSlides2(slideIndex2, slideNumber);
    }
}
function showSlides2(n, slideNumber) {
    var i;
    var slides = document.getElementsByClassName("column");
    if (slideNumber == 1) {
        if (n == 0) {
            for (i = 0; i < slides.length; i++) {
                if (i < 5) {
                    slides[i].style.display = "inline-block";
                } else {
                    slides[i].style.display = "none";
                }
            }
        } else {
            for (i = 0; i < slides.length; i++) {
                if (i < 5) {
                    slides[i].style.display = "none";
                } else {
                    slides[i].style.display = "inline-block";
                }
            }
        }
    }
}
function onMouseOver(n) {
    var columnimg = document.getElementsByClassName("column-img");
    var columntext = document.getElementsByClassName("column-link-text");
    var hoverstate = document.getElementsByClassName("column-hover");
    var i;
    for (i = 0; i < columnimg.length; i++) {
        if (n - 1 == i) {
            columnimg[i].style.display = "none";
            columntext[i].style.display = "none";
            hoverstate[i].style.display = "block";
            break;
        }
    }
}
function onMouseOut(n) {
    var columnimg = document.getElementsByClassName("column-img");
    var columntext = document.getElementsByClassName("column-link-text");
    var hoverstate = document.getElementsByClassName("column-hover");
    var i;
    for (i = 0; i < columnimg.length; i++) {
        if (n - 1 == i) {
            columnimg[i].style.display = "block";
            columntext[i].style.display = "block";
            hoverstate[i].style.display = "none";
            break;
        }
    }
}



function loadTabContent() {

    var tabContent = document.querySelectorAll(".tab-content");
    tabContent.forEach(function (tab) {
        tab.addEventListener("click", () => {
            removeActiveTab(tabContent);
            tab.classList.add("tab-active");
        });
    });
}

function removeActiveTab(tabs) {
    tabs.forEach(function (e) {
        e.classList.remove("tab-active");
    });
}

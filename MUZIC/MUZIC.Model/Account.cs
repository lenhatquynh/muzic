﻿namespace MUZIC.Model
{
    public class Account
    {

        public string Username { get; set; } = null!;
        public bool IsAdmin { get; set;}
        public DateTime Date { get; set;}
        public int Status { get; set;}
        public string Avatar { get; set; } = null!;
        public string DisplayName { get; set; } = null!;
        public string Email { get; set; } = null!;
    }
}
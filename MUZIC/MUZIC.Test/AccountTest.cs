using System.Threading.Tasks;

namespace MUZIC.Test;

[TestFixture]
public class AccountTest
{
    [SetUp]
    public void Setup()
    {
        MUZIC.App.TestHelper.Enable();
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "account/register.json" })]
    public void Register(JsonElement[] p)
    {
        Assert.Ignore();

        string username = p[0].ToString();
        string password = p[1].ToString();
        string email = p[2].ToString();

        Assert.DoesNotThrowAsync(async () =>
        {
            await AccountService.Register(username, password, email);
        });
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "account/login.json" })]
    public void Login(JsonElement[] p)
    {

        Assert.Ignore();

        string username = p[0].ToString();
        string password = p[1].ToString();

        Assert.DoesNotThrowAsync(async () =>
        {
            await AccountService.Login(username, password);
        });
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "account/get.json" })]
    public async Task GetAccount(JsonElement[] p)
    {
        Assert.Ignore();

        string username = p[0].ToString();
        string expectedUsername = p[1].ToString();

        Account? account = await AccountService.Get(username);
        if (expectedUsername == "null")
        {
            Assert.IsNull(account?.Username);
        } else
        Assert.AreEqual(expectedUsername, account?.Username);
    }
}
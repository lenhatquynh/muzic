﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MUZIC.Test
{
    [TestFixture]
    public class FavoriteTest
    {
        [SetUp]
        public void Setup()
        {
            MUZIC.App.TestHelper.Enable();
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "favorite/getFavoriteSong.json" })]
        public async Task GetSong(JsonElement[] p)
        {
            Assert.Ignore();

            string username = p[0].ToString();
            int expectedNumberElement = p[1].GetInt32();

            List<Song>? songs = await FavoriteService.GetFavoriteSong(username);
            int ele = songs.Count;
            Assert.GreaterOrEqual(ele, expectedNumberElement);
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "favorite/getFavoritePlaylist.json" })]
        public async Task GetFavoritePlaylist(JsonElement[] p)
        {
            Assert.Ignore();

            string username = p[0].ToString();
            int expectedNumberElement = p[1].GetInt32();

            List<Playlist>? playlists = await FavoriteService.GetFavoritePlaylist(username);
            int ele = playlists.Count;
            Assert.GreaterOrEqual(ele, expectedNumberElement);
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "favorite/isSongFavorite.json" })]
        public async Task IsSongFavorited(JsonElement[] p)
        {
            Assert.Ignore();

            string username = p[0].ToString();
            int id = p[1].GetInt32();
            bool expected = p[2].GetBoolean();

            bool result = await FavoriteService.IsSongFavorited(username, id);

            Assert.AreEqual(expected, result);
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "favorite/enableSong.json" })]
        public async Task EnableSong(JsonElement[] p)
        {
            Assert.Ignore();

            string username = p[0].ToString();
            int id = p[1].GetInt32();
            bool expected = p[2].GetBoolean();

            bool result = await FavoriteService.enableSong(username, id);

            Assert.AreEqual(expected, result);
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "favorite/disableSong.json" })]
        public async Task DisableSong(JsonElement[] p)
        {
            Assert.Ignore();
            string username = p[0].ToString();
            int id = p[1].GetInt32();
            bool expected = p[2].GetBoolean();

            bool result = await FavoriteService.disableSong(username, id);

            Assert.AreEqual(expected, result);
        }

    }
}

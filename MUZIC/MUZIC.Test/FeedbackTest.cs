namespace MUZIC.Test;

[TestFixture]
public class FeedbackTest
{
    [SetUp]
    public void Setup()
    {
        MUZIC.App.TestHelper.Enable();
    }

    [TestCaseSource(typeof(Util), "GetData", new object[] { "feedback/send.json" })]
    public void Send(JsonElement[] p)
    {
        Assert.Ignore();
        string username = p[0].GetString()!, title = p[1].GetString()!, detail = p[2].GetString()!;
        Assert.DoesNotThrowAsync(async () =>
        {
            await FeedbackService.Send(username, title, detail);
        });
    }
}
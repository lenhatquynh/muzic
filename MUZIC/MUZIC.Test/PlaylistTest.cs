﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MUZIC.Test
{
    [TestFixture]

    public class PlaylistTest
    {
        [SetUp]
        public void Setup()
        {
            MUZIC.App.TestHelper.Enable();
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "playlist/getPlaylist.json" })]
        public async Task GetPlaylist(JsonElement[] p)
        {
            Assert.Ignore();

            int id = p[0].GetInt32();
            Playlist? playlist = await PlaylistService.GetPlaylist(id);
            if (p[1].ToString() == "")
            {
                Assert.IsNull(playlist);
            }
            else
            {
                Assert.AreEqual(p[1].GetInt32(), playlist?.Id);
            }
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "playlist/getTop.json" })]
        public async Task GetTopPlaylist(JsonElement[] p)
        {
            Assert.Ignore();

            int NumberTop = p[0].GetInt32();
            int expected = p[1].GetInt32();
            List<Playlist>? playlist = await PlaylistService.GetTop(NumberTop);
            Assert.LessOrEqual(playlist?.Count, expected);
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "playlist/getTitle.json" })]
        public async Task GetTitle(JsonElement[] p)
        {
            Assert.Ignore();

            string title = p[0].ToString();
            string NumberCollection = p[1].ToString();
            List<Playlist>? playlist = await PlaylistService.GetTitle(title);
            if (p[1].ToString() == "null")
            {
                Assert.IsNull(playlist);
            }
            else
            {
                Assert.AreEqual(NumberCollection, playlist?[0].Title);
            }
        }


        [TestCaseSource(typeof(Util), "GetData", new object[] { "playlist/getCollection.json" })]
        public async Task GetCollection(JsonElement[] p)
        {
            string username = p[0].ToString();
            string NumberCollection = p[1].ToString();
            List<Playlist>? playlist = await PlaylistService.GetCollection(username);
            Assert.LessOrEqual(playlist?.Count, int.Parse(NumberCollection));
        }

        [TestCaseSource(typeof(Util), "GetData", new object[] { "playlist/getDuration.json" })]
        public async Task GetDuration(JsonElement[] p)
        {
            Assert.Ignore();

            int id = p[0].GetInt32();
            int expectedDuration = p[1].GetInt32();
            int duration = await PlaylistService.GetDuration(id);
            Assert.GreaterOrEqual(duration, expectedDuration, "duration "+duration);
        }
    }
}

USE [master]
GO
/****** Object:  Database [MUZIC]    Script Date: 3/5/2022 12:11:44 AM ******/
CREATE DATABASE [MUZIC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MUZIC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MUZIC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MUZIC] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MUZIC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MUZIC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MUZIC] SET ARITHABORT OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MUZIC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MUZIC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MUZIC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MUZIC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MUZIC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MUZIC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MUZIC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MUZIC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MUZIC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MUZIC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MUZIC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MUZIC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MUZIC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MUZIC] SET RECOVERY FULL 
GO
ALTER DATABASE [MUZIC] SET  MULTI_USER 
GO
ALTER DATABASE [MUZIC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MUZIC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MUZIC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MUZIC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MUZIC] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MUZIC] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'MUZIC', N'ON'
GO
ALTER DATABASE [MUZIC] SET QUERY_STORE = OFF
GO
USE [MUZIC]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/5/2022 12:11:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NULL,
	[IsAdmin] [bit] NULL,
	[Date] [datetime] NULL,
	[Status] [int] NULL,
	[Avatar] [nvarchar](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 3/5/2022 12:11:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[Id] [int] IDENTITY(1,1) not null,
	[Username] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Detail] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Priority] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Playlist]    Script Date: 3/5/2022 12:11:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Playlist](
	[Id] [int] IDENTITY(1,1) not null,
	[Username] [nvarchar](100) NOT NULL,
	[Thumbnail] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[IsPublic] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistSong]    Script Date: 3/5/2022 12:11:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistSong](
	[PlaylistId] [int] NOT NULL,
	[SongId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistTracking]    Script Date: 3/5/2022 12:11:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistTracking](
	[Username] [nvarchar](100) NOT NULL,
	[SongId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Song]    Script Date: 3/5/2022 12:11:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY,
	[Username] [nvarchar](100) NOT NULL,
	[Src] [nvarchar](max) NULL,
	[Thumbnail] [nvarchar](max) NULL,
	[Cover] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Artist] [nvarchar](max) NULL,
	[Genre] [nvarchar](max) NULL,
	[Lyrics] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Duration] [int] NOT NULL,
	[IsPublic] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SongTracking]    Script Date: 3/5/2022 12:11:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SongTracking](
	[Username] [nvarchar](100) NOT NULL,
	[SongId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'admin', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 1, CAST(N'2022-03-04T23:14:25.150' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Admin Nè', N'admin@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'canhdm', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.197' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Poi Minh Cảnh', N'canhdm@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duydn', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-02-26T15:50:59.090' AS DateTime), 0, N'/img/defaultAvatar.jpg', NULL, NULL)
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duydnt', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.660' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Đặng Ngọc Tùng Duy', N'duydnt@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duynk', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.697' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Khánh Duy', N'duynk@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'quynhln', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.337' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Lê Nhật Quỳnh', N'quynhln@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'vuna', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 0, CAST(N'2022-03-04T23:14:25.497' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Anh Vũ', N'vuna@gmail.com')
GO
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (1, N'canhdm', N'Nghe Nhạc Bị Lỗi', N'Không nghe được nhạc', CAST(N'2022-03-04T23:15:21.713' AS DateTime), 0)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (2, N'quynhln', N'Đổi Mật Khẩu', N'Không đổi mật khẩu được', CAST(N'2022-03-04T23:15:21.757' AS DateTime), 1)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (3, N'vuna', N'Upload Nhạc', N'Không upload nhạc được', CAST(N'2022-03-04T23:15:21.787' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
GO
SET IDENTITY_INSERT [dbo].[Playlist] ON 

INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (1, N'minhcanh', N'loi-bai-hat-am-tham-ben-em-ca-si-son-tung-mtp4.png (480×480) (maychuvietnam.com.vn)', N'Sơn Tùng M-TP Collection', N'Playlist được ra mắt vào những năm thập niên cuối thế kỉ 21...', CAST(N'2022-03-04T15:30:00.000' AS DateTime), 1)
INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (2, N'nhatquynh', N'https://tophinhanhdep.com/wp-content/uploads/2021/10/Thumbnail-Wallpapers.jpg', N'Jack Collection', N'Bộ sưu tập 51 hình nền và hình nền Hình thu nhỏ hàng đầu có sẵn để tải xuống miễn phí. Chúng tôi hy vọng bạn thích bộ sưu tập hình ảnh HD ngày càng tăng của chúng tôi để sử dụng làm hình nền hoặc màn hình chính cho điện thoại thông minh hoặc máy tính của bạn. Vui lòng liên hệ chúng tôi nếu bạn muốn xuất bản một Hình nhỏ hình nền trên trang web của chúng tôi.', CAST(N'2022-03-04T15:30:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Playlist] OFF
GO
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 1)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 2)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 3)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 4)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 5)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (1, 6)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 1)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 2)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 3)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 4)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 5)
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId]) VALUES (2, 6)
GO
INSERT [dbo].[PlaylistTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-02T00:00:00.000' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 2, CAST(N'2022-11-02T00:00:00.000' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 3, CAST(N'2022-01-02T00:00:00.000' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-05-01T00:00:00.000' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 2, CAST(N'2022-03-05T00:02:09.090' AS DateTime))
GO
INSERT [dbo].[Song] ([Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (N'minhcanh', N'https://ik.imagekit.io/nav26/MUZIC/song/ENGLISH_I_Wanna__Go_Home_KonoSuba__Akane_Sasu_Sora__3khnVgHth5R1.mp3?ik-sdk-version=javascript-1.4.3&updatedAt=1646412304358', N'https://yt3.ggpht.com/ytc/AKedOLRkY5n3Hd-EXXEpeUPp4INtDJTT_awisaAOhndN1g=s900-c-k-c0x00ffffff-no-rj', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Flaodong.vn%2Fgiai-tri%2Fson-tung-m-tp-dung-vi-tri-bao-nhieu-trong-top-nam-than-dep-nhat-chau-a-765130.ldo&psig=AOvVaw31EZJba7Iw9zueN25H6ZQU&ust=1646225227850000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCICbhYL5pPYCFQAAAAAdAAAAABAI', N'Muộn rồi mà sao còn', N'Son Tung MTP', N'VPop', N'"Muộn rồi mà sao còn
Nhìn lên trần nhà rồi quay ra, lại quay vào
Nằm trằn trọc vậy đến sáng mai
Ôm tương tư nụ cười của ai đó
Làm con tim ngô nghê như muốn khóc oà
Vắt tay lên trên trán mơ mộng
Được đứng bên em trong nắng xuân hồng
Một giờ sáng
Trôi qua trôi nhanh kéo theo ưu phiền miên man
Âm thầm gieo tên em vẽ lên hi vọng
Đúng là yêu thật rồi
Còn không thì hơi phí này
Cứ thế loanh quanh, loanh quanh, loanh quanh
Lật qua lật lại hai giờ"', N'Đây là bài hát của Sơn Tùng M-TP', CAST(N'2022-01-03T00:00:00.000' AS DateTime), 180, 1)
INSERT [dbo].[Song] ([Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic]) VALUES (N'nhatquynh', N'https://ik.imagekit.io/nav26/MUZIC/song/CHiCO_with_HoneyWorks%E5%B9%B8%E3%81%9B_WksW1gKxjG.mp3?ik-sdk-version=javascript-1.4.3&updatedAt=1646405593299', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUClyA28-01x4z60eWQ2kiNbA&psig=AOvVaw31EZJba7Iw9zueN25H6ZQU&ust=1646225227850000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCICbhYL5pPYCFQAAAAAdAAAAABAD', N'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUClyA28-01x4z60eWQ2kiNbA&psig=AOvVaw31EZJba7Iw9zueN25H6ZQU&ust=1646225227850000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCICbhYL5pPYCFQAAAAAdAAAAABAD', N'Âm thầm bên em', N'Son Tung MTP', N'Vpop', NULL, N'"Khi bên anh em thấy điều chi?
Khi bên anh em thấy điều gì?
Nước mắt rơi gần kề làn mi
Chẳng còn những giây phút
Chẳng còn những ân tình
Gió mang em rời xa nơi đây
Khi xa anh em nhớ về ai?
Khi xa anh em nhớ một người
Chắc không phải một người như anh
Người từng làm em khóc
Người từng khiến em buồn
Buông bàn tay, rời xa lặng thinh bước đi
Hạt mưa rơi bủa vây trái tim hiu quạnh
Ngàn yêu thương vụt tan bỗng xa
Người từng nói ở bên cạnh anh mỗi khi anh buồn
Cớ sao giờ lời nói kia như gió bay
Đừng bỏ rơi bàn tay ấy bơ vơ mà
Một mình anh lặng im chốn đây
Yêu em âm thầm bên em
Yêu thương không còn nơi đây
Anh mang tình buồn theo mây
Cơn mơ về mong manh câu thề
Tan trôi qua mau quên đi phút giây
Mưa rơi trên đôi mi qua lối vắng
Ánh sáng mờ buông lơi làn khói trắng
Bóng dáng em, nụ cười ngày hôm qua, kí ức có ngủ quên chìm trong màn sương đắng? (Anh làm em khóc)
Anh nhớ giọt nước mắt sâu lắng (anh khiến em buồn)
Anh nhớ nỗi buồn của em ngày không nắng
Buông bàn tay, rời xa lặng thinh bước đi
Hạt mưa rơi bủa vây trái tim hiu quạnh
Ngàn yêu thương vụt tan bỗng xa
Người từng nói ở bên cạnh anh mỗi khi anh buồn
Cớ sao giờ lời nói kia như gió bay?
Bàn tay bơ vơ mà
Cầm bông hoa chờ mong nhớ thương
Làm sao quên người ơi, tình anh mãi như hôm nào
Vẫn yêu người và vẫn mong em về đây
Giọt nước mắt tại sao cứ lăn rơi hoài?
Ở bên anh chỉ có đớn đau
Thì anh xin nhận hết ngàn đau đớn để thấy em cười
Dẫu biết giờ người đến không như giấc mơ
Yêu em âm thầm bên em
Yêu em âm thầm bên em
Thì anh xin nhận hết ngàn đau đớn để thấy em cười
Dẫu biết giờ người đến không như giấc mơ
Yêu em âm thầm bên em"', CAST(N'2021-03-02T00:00:00.000' AS DateTime), 150, 1)
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-02T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 2, CAST(N'2022-11-02T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 3, CAST(N'2022-01-02T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-05-01T00:00:00.000' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 2, CAST(N'2022-03-05T00:03:06.773' AS DateTime))
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ('/img/defaultAvatar.jpg') FOR [Avatar]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((0)) FOR [Priority]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT ('/img/defaultPlaylistThumbnail.jpg') FOR [Thumbnail]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT ((1)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[PlaylistTracking] ADD  CONSTRAINT [DF_PlaylistTracking_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Thumbnail]  DEFAULT ('/img/defaultSongThumbnail.jpg') FOR [Thumbnail]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Cover]  DEFAULT ('/img/defaultSongCover.jpg') FOR [Cover]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_IsPublic]  DEFAULT ((1)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[SongTracking] ADD  CONSTRAINT [DF_SongTracking1_Date]  DEFAULT (getdate()) FOR [Date]
GO
USE [master]
GO
ALTER DATABASE [MUZIC] SET  READ_WRITE 
GO

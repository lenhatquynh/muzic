USE [master]
GO
/****** Object:  Database [MUZIC]    Script Date: 3/22/2022 6:13:17 PM ******/
CREATE DATABASE [MUZIC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MUZIC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MUZIC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\MUZIC_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MUZIC] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MUZIC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MUZIC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MUZIC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MUZIC] SET ARITHABORT OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MUZIC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MUZIC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MUZIC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MUZIC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MUZIC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MUZIC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MUZIC] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MUZIC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MUZIC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MUZIC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MUZIC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MUZIC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MUZIC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MUZIC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MUZIC] SET RECOVERY FULL 
GO
ALTER DATABASE [MUZIC] SET  MULTI_USER 
GO
ALTER DATABASE [MUZIC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MUZIC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MUZIC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MUZIC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MUZIC] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MUZIC] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'MUZIC', N'ON'
GO
ALTER DATABASE [MUZIC] SET QUERY_STORE = OFF
GO
USE [MUZIC]
GO
/****** Object:  UserDefinedFunction [dbo].[rmvAccent]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rmvAccent] (@text nvarchar(max))
RETURNS nvarchar(max)
AS
BEGIN
	SET @text = LOWER(@text)
	DECLARE @textLen int = LEN(@text)
	IF @textLen > 0
	BEGIN
		DECLARE @index int = 1
		DECLARE @lPos int
		DECLARE @SIGN_CHARS nvarchar(100) = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệếìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵýđð'
		DECLARE @UNSIGN_CHARS varchar(100) = 'aadeoouaaaaaaaaaaaaaaaeeeeeeeeeeiiiiiooooooooooooooouuuuuuuuuuyyyyydd'

		WHILE @index <= @textLen
		BEGIN
			SET @lPos = CHARINDEX(SUBSTRING(@text,@index,1),@SIGN_CHARS)
			IF @lPos > 0
			BEGIN
				SET @text = STUFF(@text,@index,1,SUBSTRING(@UNSIGN_CHARS,@lPos,1))
			END
			SET @index = @index + 1
		END
	END
	RETURN @text
END
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](max) NULL,
	[IsAdmin] [bit] NULL,
	[Date] [datetime] NULL,
	[Status] [int] NULL,
	[Avatar] [nvarchar](max) NULL,
	[DisplayName] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Detail] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Priority] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Playlist]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Playlist](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Thumbnail] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[IsPublic] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistSong]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistSong](
	[PlaylistId] [int] NOT NULL,
	[SongId] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PlaylistId] ASC,
	[SongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaylistTracking]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistTracking](
	[Username] [nvarchar](100) NOT NULL,
	[PlaylistId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Song]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Song](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Src] [nvarchar](max) NULL,
	[Thumbnail] [nvarchar](max) NULL,
	[Cover] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Artist] [nvarchar](max) NULL,
	[Genre] [nvarchar](max) NULL,
	[Lyrics] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Duration] [int] NOT NULL,
	[IsPublic] [bit] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SongTracking]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SongTracking](
	[Username] [nvarchar](100) NOT NULL,
	[SongId] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WebTracking]    Script Date: 3/22/2022 6:13:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebTracking](
	[Username] [nvarchar](100) NULL,
	[ContextURL] [nvarchar](100) NULL,
	[Date] [datetime] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'admin', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 1, CAST(N'2022-03-21T03:30:30.150' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Admin Nè', N'admin@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'canhdm', N'gr24IbM-3jnFpwUYK34Wzv0HUk_LHunYuh3Ksu0IKko', 0, CAST(N'2022-03-21T15:35:34.363' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Đặng Minh Cảnh', N'canhdm@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duydnt', N'gr24IbM-3jnFpwUYK34Wzv0HUk_LHunYuh3Ksu0IKko', 0, CAST(N'2022-03-21T15:36:44.380' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Đặng Ngọc Tùng Duy', N'duydnt@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'duynk', N'gr24IbM-3jnFpwUYK34Wzv0HUk_LHunYuh3Ksu0IKko', 0, CAST(N'2022-03-21T15:36:23.420' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Khánh Duy', N'duynk@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'lanltt', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 1, CAST(N'2022-03-21T15:34:32.303' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Lê Thị Thu Lan', N'lanltt@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'quynhln', N'gr24IbM-3jnFpwUYK34Wzv0HUk_LHunYuh3Ksu0IKko', 0, CAST(N'2022-03-21T15:35:58.367' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Lê Nhật Quỳnh', N'quynhln@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'vinhnd', N'8RACcxs-Ou8oyC5yUrDaLcFOckNbbduJJZxmBd-hhO4', 1, CAST(N'2022-03-21T15:35:05.273' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Đình Vinh', N'vinhnd@gmail.com')
INSERT [dbo].[Account] ([Username], [Password], [IsAdmin], [Date], [Status], [Avatar], [DisplayName], [Email]) VALUES (N'vuna', N'gr24IbM-3jnFpwUYK34Wzv0HUk_LHunYuh3Ksu0IKko', 0, CAST(N'2022-03-21T15:37:10.213' AS DateTime), 0, N'/img/defaultAvatar.jpg', N'Nguyễn Anh Vũ', N'vuna@gmail.com')
GO
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (1, N'canhdm', N'Lỗi Nhạc', N'Đang nghe nhạc tự nhiên bị giật', CAST(N'2022-03-21T20:01:10.903' AS DateTime), 2)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (2, N'duydnt', N'Upload Nhạc', N'Không thể upload nhạc', CAST(N'2022-03-21T20:01:39.880' AS DateTime), 2)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (3, N'duynk', N'Add Song To Playlist', N'Không thêm nhạc vào playlist được', CAST(N'2022-03-21T20:02:11.800' AS DateTime), 3)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (4, N'quynhln', N'Đổi mật khẩu', N'Không thể đổi mật khẩu', CAST(N'2022-03-21T20:02:33.927' AS DateTime), 1)
INSERT [dbo].[Feedback] ([Id], [Username], [Title], [Detail], [Date], [Priority]) VALUES (5, N'vuna', N'Mp3 Player', N'Không thể next bài hát', CAST(N'2022-03-21T20:03:20.100' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
GO
SET IDENTITY_INSERT [dbo].[Playlist] ON 

INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (1, N'canhdm', N'/img/upload-cover.jpg', N'Playlist của Đặng Minh Cảnh', N'Đây là những bài hát mà Đặng Minh Cảnh thích nghe', CAST(N'2022-03-21T19:45:31.547' AS DateTime), 1)
INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (2, N'duydnt', N'/img/upload-cover.jpg', N'Playlist của Đặng Ngọc Tùng Duy', N'Đây là những bài hát mà Đặng Ngọc Tùng Duy thích nghe', CAST(N'2022-03-21T19:46:37.570' AS DateTime), 1)
INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (3, N'duynk', N'/img/upload-cover.jpg', N'Playlist của Nguyễn Khánh Duy', N'Đây là những bài hát mà Nguyễn Khánh Duy thích nghe', CAST(N'2022-03-21T19:47:39.870' AS DateTime), 1)
INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (4, N'quynhln', N'/img/upload-cover.jpg', N'Playlist của Lê Nhật Quỳnh', N'Đây là những bài hát mà Lê Nhật Quỳnh thích nghe', CAST(N'2022-03-21T19:48:16.060' AS DateTime), 1)
INSERT [dbo].[Playlist] ([Id], [Username], [Thumbnail], [Title], [Description], [Date], [IsPublic]) VALUES (5, N'vuna', N'/img/upload-cover.jpg', N'Playlist của Nguyễn Anh Vũ', N'Đây là những bài hát mà Nguyễn Anh Vũ thích nghe', CAST(N'2022-03-21T19:48:55.923' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Playlist] OFF
GO
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 1, CAST(N'2022-03-21T19:49:51.870' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 2, CAST(N'2022-03-21T19:49:54.257' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 3, CAST(N'2022-03-21T19:49:56.053' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 4, CAST(N'2022-03-21T19:49:57.877' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 5, CAST(N'2022-03-21T19:50:00.300' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 6, CAST(N'2022-03-21T19:50:02.253' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 7, CAST(N'2022-03-21T19:50:04.547' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 8, CAST(N'2022-03-21T19:54:59.683' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 9, CAST(N'2022-03-21T19:55:01.760' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 10, CAST(N'2022-03-21T19:55:03.930' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 11, CAST(N'2022-03-21T19:55:07.720' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 12, CAST(N'2022-03-21T19:55:09.543' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 13, CAST(N'2022-03-21T19:55:11.287' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (1, 14, CAST(N'2022-03-21T19:55:13.440' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 8, CAST(N'2022-03-21T19:50:06.997' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 9, CAST(N'2022-03-21T19:50:09.203' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 10, CAST(N'2022-03-21T19:50:11.423' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 11, CAST(N'2022-03-21T19:50:13.487' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 12, CAST(N'2022-03-21T19:50:15.367' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 13, CAST(N'2022-03-21T19:50:17.567' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 14, CAST(N'2022-03-21T19:50:20.133' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 15, CAST(N'2022-03-21T19:55:17.280' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 16, CAST(N'2022-03-21T19:55:19.570' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 17, CAST(N'2022-03-21T19:55:21.240' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 18, CAST(N'2022-03-21T19:55:23.187' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 19, CAST(N'2022-03-21T19:55:25.007' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 20, CAST(N'2022-03-21T19:55:28.020' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (2, 21, CAST(N'2022-03-21T19:55:29.937' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 15, CAST(N'2022-03-21T19:50:32.650' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 16, CAST(N'2022-03-21T19:50:35.530' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 17, CAST(N'2022-03-21T19:50:37.527' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 18, CAST(N'2022-03-21T19:50:39.540' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 19, CAST(N'2022-03-21T19:50:41.537' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 20, CAST(N'2022-03-21T19:50:43.570' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 21, CAST(N'2022-03-21T19:50:45.570' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 22, CAST(N'2022-03-21T19:55:39.657' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 23, CAST(N'2022-03-21T19:55:42.120' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 24, CAST(N'2022-03-21T19:56:05.743' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 25, CAST(N'2022-03-21T19:56:26.017' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 26, CAST(N'2022-03-21T19:56:28.817' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 27, CAST(N'2022-03-21T19:56:30.737' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (3, 28, CAST(N'2022-03-21T19:56:32.840' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 22, CAST(N'2022-03-21T19:50:48.587' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 23, CAST(N'2022-03-21T19:50:50.623' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 24, CAST(N'2022-03-21T19:50:52.777' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 25, CAST(N'2022-03-21T19:50:54.750' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 26, CAST(N'2022-03-21T19:50:57.503' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 27, CAST(N'2022-03-21T19:50:59.360' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 28, CAST(N'2022-03-21T19:51:01.220' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 29, CAST(N'2022-03-21T19:56:35.197' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 30, CAST(N'2022-03-21T19:56:37.180' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 31, CAST(N'2022-03-21T19:56:39.560' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 32, CAST(N'2022-03-21T19:56:41.797' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 33, CAST(N'2022-03-21T19:56:43.910' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 34, CAST(N'2022-03-21T19:56:46.420' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (4, 35, CAST(N'2022-03-21T19:56:51.797' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 1, CAST(N'2022-03-21T19:57:03.970' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 2, CAST(N'2022-03-21T19:57:05.820' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 3, CAST(N'2022-03-21T19:57:07.453' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 4, CAST(N'2022-03-21T19:57:09.257' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 5, CAST(N'2022-03-21T19:57:10.710' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 6, CAST(N'2022-03-21T19:57:12.777' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 7, CAST(N'2022-03-21T19:57:16.513' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 29, CAST(N'2022-03-21T19:51:03.207' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 30, CAST(N'2022-03-21T19:51:08.210' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 31, CAST(N'2022-03-21T19:51:28.833' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 32, CAST(N'2022-03-21T19:51:31.343' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 33, CAST(N'2022-03-21T19:51:34.620' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 34, CAST(N'2022-03-21T19:51:36.687' AS DateTime))
INSERT [dbo].[PlaylistSong] ([PlaylistId], [SongId], [Date]) VALUES (5, 35, CAST(N'2022-03-21T19:51:39.703' AS DateTime))
GO
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:03:39.670' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 2, CAST(N'2022-03-21T20:03:50.210' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:04:03.743' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 4, 2, CAST(N'2022-03-21T20:04:04.990' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:04:13.907' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:04:17.960' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:04:43.803' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:04:46.677' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:04:57.213' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:05:22.830' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 2, CAST(N'2022-03-21T20:05:31.200' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:05:35.697' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 4, 2, CAST(N'2022-03-21T20:05:37.123' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:05:55.237' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 2, CAST(N'2022-03-21T20:05:56.383' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:12.130' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'vuna', 1, 2, CAST(N'2022-03-21T20:06:14.160' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:17.243' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:27.807' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'vuna', 2, 2, CAST(N'2022-03-21T20:06:30.237' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:56.470' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:57.177' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:57.437' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:57.803' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:58.200' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:58.607' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:58.757' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:59.113' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:00.570' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:01.637' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:01.907' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:02.173' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:02.337' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:02.573' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:02.713' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:03.123' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:03.537' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:03.953' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:08:04.430' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:05.430' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:05.837' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:06.237' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:06.433' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:06.620' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:07.040' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:07.430' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:07.630' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:08.010' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:08.440' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:08:08.600' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:49.310' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:49.710' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:50.040' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:50.443' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:50.710' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:50.873' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:51.130' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:51.263' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:51.677' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 2, CAST(N'2022-03-21T20:08:52.017' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:52.543' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:52.913' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:54.260' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:54.973' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:55.930' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:56.290' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:56.653' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:57.077' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:57.370' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:57.653' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:57.847' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:41.000' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:41.457' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:41.790' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:45.480' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:46.057' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:46.300' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:46.717' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:47.457' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:47.647' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:13.470' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:13.920' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:14.200' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:14.410' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:14.540' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:14.930' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:15.220' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:15.373' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:15.630' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:15.773' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:16.023' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:16.193' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:16.450' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:16.627' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:16.890' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:17.060' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:17.287' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:17.517' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:17.757' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:17.907' AS DateTime))
GO
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:18.060' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:34.690' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:35.290' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:35.560' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:35.677' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:36.040' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:36.420' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:36.773' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:38.123' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:38.727' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:38.943' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:39.060' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:39.290' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:39.523' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:39.637' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:39.900' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:40.010' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:40.253' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:40.453' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:40.677' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:40.813' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:41.047' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:41.223' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:41.380' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-22T18:11:41.600' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:17.790' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:18.660' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:19.033' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:19.283' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:19.443' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:19.630' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:19.790' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:20.143' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:20.397' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:20.600' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:20.763' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:20.983' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:21.200' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:21.457' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:21.623' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:21.880' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:22.033' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:22.210' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:22.480' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:23.000' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 2, CAST(N'2022-03-22T18:12:23.823' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:24.293' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:24.563' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:24.713' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:24.957' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:25.113' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:25.377' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:25.500' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:26.117' AS DateTime))
INSERT [dbo].[PlaylistTracking] ([Username], [PlaylistId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-22T18:12:26.677' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Song] ON 

INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (1, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/LacNhauCoPhaiMuonDoi-ERIKST319_DGIpB1nRJ.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Lạc nhau có phải muôn đời', N'Erik', N'Nhạc trẻ', N'Thế giới bé thế nào
Mình gặp nhau có phải muôn đời
Ngày mà người mang đến một khúc hát không thể quên
Bài hát với những mơ mộng
Bài hát với những hy vọng
Cho đời ta chút vui
Cho đời ta chút thương
.
Thế giới lớn thế nào
Mình lạc nhau có phải muôn đời
Dòng người vội vàng qua người sẽ đứng nơi đâu chờ ta
Chờ giữa quán xá ven đường
Chờ cuối góc phố năm nào
Xin chờ ta chút thôi. Chút thôi!

Bình minh đến đón ánh nắng sớm mình hẹn hò chốn thân *** ngày xưa
Hoàng hôn xuống dưới góc phố vắng mình cười nói vu vơ bao chuyện qua
Vì tình yêu ta trao người hết xin người đừng khiến tim này mong nhớ
Hãy hứa sẽ mãi bên ta

Ngày vẫn thế ấm áp tiếng nói nụ cười người với bao nhiêu buồn vui
Chiều buông nắng gió vẫn khẽ hát thì thầm những khúc ca cho tình nhân
Làm sao khi con tim nhỏ bé mơ hoài những phút giây ôm lấy người
Đời như một giấc mơ mãi không tàn vì người...', N'Lạc nhau có phải muôn đời là Chờ em đến ngày mai OST', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 283, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (2, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/XichThemChutXTCRemix-GroovieLaThangTlinhMCKRPT_S9zgqqXPH.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Xích thêm chút', N'Groovie Lã Thắng, Tlinh, MCK', N'Nhạc trẻ', N'[Groovie:]
Em ơi ta hãy yêu như chỉ có đêm nay được yêu
Cho anh say mắt môi em lại có nhạc hay để chill
Sâu trong tâm trí anh chính là nơi em thường tìm đến
Rồi tan vào trong giấc mơ
Ai cho em chút thơ ngọt ngào ấm trong từng hơi thở
Share cho em giấc mơ như bình minh xua tan mây mờ
Yêu em như redbull vẫn tìm đến trong ly cùng jagerrrrrr
Hey girl
Có thể bay bằng hai cách
Bằng máy bay hoặc là maybach
Chẳng cần cánh đâu anh là superman
Chẳng phải người máy nhưng anh có duracell
Em ***y girl bao anh xin chết
Anh chỉ có một trái tim xanh green heart phải nói là tín đét
Oh baby girl tonight u are my princess
Ghi tên em vào guinness của anh, của anh
Nâng bàn tay của em để biết thời gian đang trôi qua rất nhanh
Hôn bờ môi của em cho cảm xúc anh không mất phanh
Qua nửa đêm trời sáng em mong anh không quên mất em
Quên làm sao lời ca long lanh trong đôi mắt xanh
Lối sống đẹp mình trippin''
Volkswagen mình flippin''
Let me see your movin''
Groovin'' with groovie (x2)

[Tlinh:]
Anh ơi ta hãy yêu như chỉ có đêm nay được yêu
Cho em say mắt môi anh lại có nhạc hay để chill
Sâu trong tâm trí em chính là nơi anh thường tìm đến
Rồi tan vào trong giấc mơ
Ai cho em chút thơ ngọt ngào ấm trong từng hơi thở
Trao cho em giấc mơ như bình minh xua tan mây mờ
Yêu anh như redbull vẫn tìm đến trong ly cùng jagerrrrrr
Hey boy
Anh bước vào căn phòng thiêu đốt mọi vật xung quanh
Con tim em nghe tiếng pop của rượu champage
Không một ai khiến em rạo rực như anh
Đặt vòng tay quanh eo này khẽ ôm from the back
U know i like that
Black hoodie with them bluejeans
Tóc messy mùi armani
Cho em lại gần một chút đi
The neckchains and the *** game
Đồng tử giãn lên như là đưa em vào cõi thần tiên yeah
Babi can u see u are all i need
Yêu anh đến chết ngất đi infinity
Bao nhiêu phiền lo cất đi
Cause u''r here with me
U''re my fantasy
U''re my xtc

[RPT MCK:]
Cause ur p****y feel so good
Nó đã khiến anh rơi vào
Vậy tình yêu là một trò chơi hay là cuộc chiến em khơi mào
Và em giấu đi mọi buồn đau ở sau làn mi
Đến khi nó phai nhạt đi
Và chẳng cần biết em đang cuồng quay ở nơi nào
Em nhìn như prada
Anh đã thấy em luôn cười balenciaga
Anh đã lỡ thương người và anh chẳng cần nhiều lời nói
Anh chỉ quan tâm trái tim em có tính yêu đậm sâu
Ta hôn nhau thật lâu
Trong club đèn màu', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 244, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (3, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/TinhYeuMauHong-HoVanQuyXam_y4WEVLAKti.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Tình yêu màu hồng', N'Hồ Văn Quý, Xám', N'Nhạc trẻ', N'Mây mang bao yêu thương gửi trao em
Ngàn khúc hát cho ngày xanh thật dịu êm
Đôi môi yêu thương trong sang còn ngát hương
Ngày yêu dấu cho tình anh sẽ mãi thương

Dịu dàng hương hoa em đến bên đời ta
Bầu trời chợt thắm mang yêu dấu vào khúc ca
Ngày xanh như hoa tình yêu sẽ không phai nhòa
Lặng nhìn cơn mơ nhẹ du em vào vần thơ

[ĐK:]
Ngàn câu ca đến bên bầu trời tình mình đẹp biết bao nhiêu
Dịu dàng như nắng mai hiền cho khóe môi được bình yên
Dịu lại một chút hương thơm cho đời ngọt ngào tiếng ca
Để em đến bên đời ta rồi dịu dàng lại hóa thiết tha

Ngàn muôn hoa thắm trong một buổi chiều tháng ba
Tình ta như hoa ngọt ngào giữa khung trời bao la
Tình yêu nhiệm màu dìu nhau qua cơn bão going
Để ta trông ngóng vào một tình yêu màu hồng

[RAP:]
Những lời ca còn đọng lại bên tai
Anh đặt nhẹ bàn tay này lên vai
Thành phố này nhỏ lại
Chỉ còn bằng đôi mắt em
Ta hòa vào nhau bằng những nốt nhạc
Thời gian đừng trôi mất đi

Góc ban công , ở lầu hai , nhạc lofi và đèn trời
Em gỡ hết đi mọi phòng thủ chẳng sợ nỗi buồn nào tràn vào
Một nụ hôn lên đôi môi
Cho con tim em ngưng đọng
Dưới bầu trời đêm trong không gian
Có hai cá thể rung động

Và rồi mọi thứ sẽ ổn thôi như những gì ta hằng mong
Em đưa tầm mắt vào bầu trời nơi vì sao phía đằng đông
Tựa vào vai anh, không phiền muộn một buổi tối chủ nhật
Thì thầm lời yêu, đan bàn tay ,đôi mi em ngủ gật
Ta cùng nghe lại những giai điệu phía bên kia bầu trời
Mang cho em con tim khao khát như một lời yêu đầu đời
Dạt dào như những con sóng
Ngập cả một đại dương xanh
Rồi rơi vào giữa tim ai chạng vạng nếu như e cũng ngỏ lời thương a

2. Bao vần thơ nhẹ ru
Khi ánh nắng vẫn còn vương
Đôi mi em vẫn thương

Nhẹ trao hết bao câu ca
Tình ta sẽ không xa
Khung trời kia dịu êm

[ĐK:]
Ngàn câu ca đến bên bầu trời tình mình đẹp biết bao nhiêu
Dịu dàng như nắng mai hiền cho khóe môi được bình yên
Dịu lại một chút hương thơm cho đời ngọt ngào tiếng ca
Để em đến bên đời ta rồi dịu dàng lại hóa thiết tha

Ngàn muôn hoa thắm trong một buổi chiều tháng ba
Tình ta như hoa ngọt ngào giữa khung trời bao la
Tình yêu nhiệm màu dìu nhau qua cơn bão going
Để ta trông ngóng vào một tình yêu màu hồng

Dịu dàng hương hoa em đến bên đời ta
Bầu trời chợt thắm mang yêu dấu vào khúc ca
Ngày xanh như hoa tình yêu sẽ không phai nhòa
Lặng nhìn cơn mơ nhẹ du em vào vần thơ

[ĐK:]
Ngàn câu ca đến bên bầu trời tình mình đẹp biết bao nhiêu
Dịu dàng như nắng mai hiền cho khóe môi được bình yên
Dịu lại một chút hương thơm cho đời ngọt ngào tiếng ca
Để em đến bên đời ta rồi dịu dàng lại hóa thiết tha

Ngàn muôn hoa thắm trong một buổi chiều tháng ba
Tình ta như hoa ngọt ngào giữa khung trời bao la
Tình yêu nhiệm màu dìu nhau qua cơn bão going
Để ta trông ngóng vào một tình yêu màu hồng
', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 294, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (4, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/DayToiCachYeu-Tlinh_fGXjGS5Eg.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Dạy tôi cách yêu', N'Tlinh', N'Nhạc trẻ', N'Tôi vẫn luôn okay một mình
Không phù hợp với đôi ba cuộc tình
Điều gì đến kệ vạn sự tuỳ duyên
Sở hữu ai? Tôi vốn đâu có quyền

Tôi vẫn luôn okay một mình
Không giỏi cho đi chỉ ích kỉ nên thực tình
Tôi không muốn phải dựa vào ai
Càng không muốn treat someone sai

Nên là tình yêu tôi nói không
Không thích cảm giác phải nhớ mong
Không muốn trói buộc ai trong một mớ hỗn độn
Mà rồi đến cuối tất cả cũng rồi sẽ lộn xộn

Nên là anh ơi nếu có thích tôi
Xin anh rời xa vì sớm thôi
Anh sẽ chứng kiến những cuộc chiến tranh đằng sau bức
Tường này và liệu anh có thấy phiền toái hay coi nó là tuyệt tác?

Somebody tell me how to love?
I''d be faithful than ever
Just tell me how to treat somebody right
I''d be grateful than ever

Somebody tell me how to love?
I''d be faithful than ever
Just tell me how to put my hopes high
I''d be grateful than ever

Tình yêu vốn đâu có luật lệ
Nhưng nếu tôi có liệu anh có mặc kệ bỏ qua
Không muốn nghe những hứa hẹn lời thề
Không muốn chỉ là một người thay thế

Tôi vốn cũng đâu có luật lệ
Tình yêu khó đoán nên tôi cần phải tự vệ
Dành cho nhau sự chấp nhận và yêu thương
Mặc dù nhiều khi chúng là thứ khiến ta mất phương hướng

Nên là tình yêu tôi nói không
Không thích cảm giác phải ngóng trông
Không muốn phải thấy một ai đi mất để tôi lại
Khổ càng không muốn thấy ai vì tôi mà cũng phải khổ

Nên là nếu có tìm đến tôi
Nếu có thực sự cần đến tôi
Xin hãy dạy tôi biết thế nào là yêu
Đừng lại rời xa để tôi mới biết mình đang yêu?

Somebody tell me how to love?
I''d be faithful than ever
Just tell me how to treat somebody right
I''d be grateful than ever

Somebody tell me how to love?
I''d be faithful than ever
Just tell me how to put my hopes high
I''d be grateful than ever', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 259, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (5, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/BoEmVaoBaloLofiVersion-TanTranFreakD_1QKxc7yy_.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Bỏ em vào balo', N'Tân Trần, Freak D', N'Nhạc trẻ', N'Đón em về nhà sau buổi chiều tăng ca
Đoạn đường vẫn thế sao hôm nay xa quá
Rồi bỗng nhiên em không muốn về nhà
Rồi bỗng nhiên anh cũng muốn đi xa
Mình sống giữa lòng Hà Nội nhộn nhịp em ơi
Mà đôi khi thấy lòng mình chơi vơi
Dòng người đảo điên đôi khi khiến ta rối bời
Hay là cứ theo anh mình đi đến nơi
Bỏ em vào balo đưa em ra khỏi thủ đô
Mình cùng rời thành phố tránh những làn khói ô tô
Xây một căn nhà gỗ ở mãi xa tận ngoại ô
Vứt hết những bão tố giữa chốn Hà Nội đông đúc xô bồ
Gạt bỏ muộn phiền đằng sau mình sống như là Đen Vâu
Về mình nuôi thêm cá buồn quá mình trồng thêm rau
Anh chẳng ngại điều gì đâu
Nước mắt mình tự mình lau
Tình cảm dù trước dù sau chỉ cần được có nhau.', N'Bài này chill phết', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 186, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (6, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/NangTho-HoangDung_dAk2ijXbW.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Nàng thơ', N'Hoàng Dũng', N'Nhạc trẻ', N'Em, ngày em đánh rơi nụ cười vào anh
Có nghĩ sau này em sẽ chờ
Và vô tư cho đi hết những ngây thơ
Anh, một người hát mãi những điều mong manh
Lang thang tìm niềm vui đã lỡ
Chẳng buồn dặn lòng quên hết những chơ vơ

Ta yêu nhau bằng nỗi nhớ chưa khô trên những bức thư
Ta đâu bao giờ có lỗi khi không nghe tim chối từ
Chỉ tiếc rằng

Em không là nàng thơ
Anh cũng không còn là nhạc sĩ mộng mơ
Tình này nhẹ như gió
Lại trĩu lên tim ta những vết hằn
Tiếng yêu này mỏng manh
Giờ tan vỡ, thôi cũng đành
Xếp riêng những ngày tháng hồn nhiên
Trả lại...

Mai, rồi em sẽ quên ngày mình khờ dại
Mong em kỷ niệm này cất lại
Mong em ngày buồn thôi ướt đẫm trên vai
Mai, ngày em sải bước bên đời thênh thang
Chỉ cần một điều em hãy nhớ

Có một người từng yêu em tha thiết vô bờ

Em không là nàng thơ
Anh cũng không còn là nhạc sĩ mộng mơ
Tình này nhẹ như gió
Lại trĩu lên tim ta những vết hằn
Tiếng yêu này mỏng manh
Giờ tan vỡ, thôi cũng đành
Xếp riêng những ngày tháng hồn nhiên
Trả hết cho em', N'Bài này chill phết', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 255, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (7, N'canhdm', N'https://ik.imagekit.io/nav26/MUZIC/song/ChiMotDemNuaThoi-RPTMCKTlinh_Qbe2oC6jK.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Chỉ một đêm nữa thôi', N'MCK, Tlinh', N'Rap Việt', N'Đôi khi đi hơi nhanh như con xe không phanh
Đôi khi làm mình lẻ loi âm thanh vang lên nghe trong tai nghe
Và tình yêu thì đã lên ngôi nhìn đêm trôi em sát bên tôi
Chạm vào em mới bất giác lên ngôi

Mọi buồn đau rồi sẽ quên thối ôi
Mấy cái tin inbox của em giờ đang ở trong đầu
Và anh đã thức biết bao nhiêu là đêm dài mong ngóng
Không cần phải nói mà sao vẫn cứ hiểu thấu lòng nhau

Same frequency I don''t wanna let u go
Yeh anh không muốn thức dậy
Anh muốn chìm trong đôi mắt nàng
Cạm bẫy vẫn đang xung quanh em

Clutching got it got it got it girl dé e
DCOD Life
Chân em mang Nike Low
You gotta pick a side

Eenie miney moe
Mình cùng ngồi ngắm trăng tàn
Ở Sài Gòn hai giờ đêm
Henni làm mắt ai mờ thêm

Yeah sau đêm nay xem là ai ở bên
Quấn một điếu thật to
Mãi giống với Thành Draw
Bọn em muốn có thật là nhiều tiền

And we gonna make some more
ACID gang gang 350
Lúc đó nhìn em thật đẹp
That''s everything I know

Mình cùng xuống phố, khi đã lên đồ
Bầu trời nhiều sao mà đêm nay vẫn tối đen
Bao nhiêu cạm vẫy, đang ở thiên đường
Nhưng mà tại sao trong tim anh có mỗi em

Và sẽ còn biết đớn đau là đến bằng này
Nếu cứ tiếp tục trên đà này
I''m so gonna *** *** *** *** fall in love with you
Yeh anh không muốn thức dậy

Anh muốn chìm trong đôi mắt nàng
Cạm bẫy vẫn đang xung quanh em
Clutching got it got it got it girl yeahh
Cho em nắm tay anh một chút được không?

Những điều sau đây xin phép được nói thật lòng (Tell me baby)
Em không chắc em đang cảm thấy yêu gì nhưng mà nhớ anh, chỉ muốn thấy anh ở bên cạnh
You got me tripping như bị rơi vào tròng
Nhưng em không thấy bản thân mình đang đề phòng (Noooo)

You make me feel safe
"Rằng mọi thứ sẽ okay"
Khi ở trong vòng tay anh
Em không muốn thức dậy (Yeh anh cũng không muốn thức dậy)

Mặc kệ thời gian này nó chảy (Mặc kệ thời gian đi babe)
Xung quanh là bao nhiêu cạm bẫy
Munchies got it yeh', N'Bài này live tại show!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 135, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (8, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/Dau_Ai_Dam_Hua_-_Czee_GYYmnZfRX.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Đâu ai dám hứa', N'Czee', N'Nhạc trẻ ', N'Chiều dần buông tan trường anh đến để chờ nàng thơ đi học ra
Thì ra em đang ngồi trên chiếc xe đạp của ai không phải anh
Anh như muốn tắt luôn công tắc gật đầu (tắt luôn công tắt gật đầu)
Vì trông thấy em đang cười đùa cùng ai khác
Cũng chẳng thể trách em chọn ai đó, hai mình đã có cái gì đâu
Người ta duyên, nhiều tiền, cao to, muôn phần hơn anh
Ôi giồi ôi, chán luôn!
Làm sao anh có thể có được người yêu xinh? (như em đó)
Làm sao anh có thể biết cảm giác em lúc này?
Chỉ anh mới thấy phút yếu đuối trong mắt em thơ ngây
Rất nhiều lần, mỗi ngày
Bên em là hạnh phúc, sao ai kia không quan tâm?
Thằng kia có hứa sẽ chăm lo, sẽ đón em mỗi chiều?
Dù cho việc đó có khó đến mức để em trông ngóng nhiều ngày
Rất nhiều ngày
Chỉ cần được nắm tay em đến muôn nơi
Đâu ai dám hứa
Đâu ai dám hứa
Hứa-ưa-ưa, hứa
Cũng chẳng thể trách em chọn ai đó, hai mình đã có cái gì đâu
Người ta duyên, nhiều tiền, cao to, muôn phần hơn anh
Ôi giồi ôi, chán luôn!
Làm sao anh có thể có được người yêu xinh?
Làm sao anh có thể biết cảm giác em lúc này?
Chỉ anh mới thấy phút yếu đuối trong mắt em thơ ngây
Rất nhiều lần, mỗi ngày
Bên em là hạnh phúc, sao ai kia không quan tâm?
Thằng kia có hứa sẽ chăm lo, sẽ đón em mỗi chiều?
Dù cho việc đó có khó đến mức để em trông ngóng nhiều ngày
Rất nhiều ngày
Chỉ cần được nắm tay em đến muôn nơi
Anh tuy nghèo thật
Không có nhiều tiền để lo cho em cuộc sống mới, no
Hôm nay là vậy
Liệu rằng ngày mai anh có được tất cả
Em có trở về bên anh?
Làm sao anh có thể biết cảm giác em lúc này?
Chỉ anh mới thấy phút yếu đuối trong mắt em thơ ngây (no, no)
Rất nhiều lần, mỗi ngày (em có trở về bên anh?)
Bên em là hạnh phúc sao ai kia không quan tâm?
Thằng kia có hứa sẽ chăm lo, sẽ đón em mỗi chiều?
Dù cho việc đó có khó đến mức để em trông ngóng nhiều ngày
Rất nhiều ngày
Chỉ cần được nắm tay em đến muôn nơi
Đâu ai dám hứa
Đâu ai dám hứa
Hứa oh-hoh
Đâu ai dám hứa', N'Bài này chill phết', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 224, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (9, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/DauNhatLaLangIm-ERIKmp3_r1wP4K5ry.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Đau nhất là lặng im', N'Erik', N'Nhạc trẻ', N'Những thật lòng
Giờ đã hóa vô vọng
Lời xin lỗi nói ra chẳng níu được tình đôi ta
Đâu ai muốn mai này
Thành quá khứ đã từng của nhau
Tình dù đậm sâu thật nhiều
Ngờ đâu cũng phải buông tay

Bởi vì khi đã yêu đâu ai biết trước
Đến một ngày mang quá nhiều vết xước
Dù rằng anh buông tay hay cố giữ chặt
Chẳng thể hiểu được điều em muốn nói

Lặng im đến lúc nào?
Giờ em muốn anh phải làm sao?
Em mang đến yên bình
Rồi thả anh xuống nơi vực sâu
Từng yêu nhau
Xem nhau là cả tương lai mai sau
Vậy mà chúng ta chẳng ở lại bên nhau
Anh chẳng biết làm gì
Vì càng bước tới em càng đi
Anh vô ý, hay em cần anh nói ra những gì
Những vết thương chưa lành
Thì đau nhất vẫn là lặng im buông tay

Lời chia tay sẽ không thể nào đúng lúc
Chẳng ai tự tin đối mặt kết thúc
Dù rằng anh buông tay hay cố giữ chặt
Chẳng thể hiểu được điều em muốn nói

Vậy là kết thúc rồi
Chẳng ai muốn cứ yêu càng đau

Coda: Dẫu biết là lần yêu này
Hai ta chẳng thể bước qua
Và anh sẽ cất giữ yêu thương tạm gác đi những vấn vương từng ngày
Chỉ mong cơn mơ này vẹn nguyên trong ta hnay và mãi mãi

Chẳng thể nào lặng im được đâu
Hãy nói ra hết dù tổn thương nhau', N'Bài này chill phết', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 286, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (10, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/ExsHateMe-BRayMasewAMee_MMnur1F8b.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Ex''s Hate Me', N'Bray, Masew, Amee', N'Rap Việt', N'All my ex’s hate me,
And I hate them back,
And it’s all because of me,
And it’s all because of me.

Anh xin được dùng bài nhạc này gửi những người yêu cũ,
Rằng là,
Nếu bây giờ em đang yên vui cùng người yêu mới,
Anh cầu chúc em có những thứ không thể tìm thấy được từ nơi anh,
Và, ngày anh nhận được thiệp đám cưới từ em nó sẽ tới nhanh,
Chúc em có thật nhiều bữa sáng trên giường ngủ,
Chúc em luôn chọn được cho mình 1 lối đi khác trên đường cũ,
Chúc em có 1 gia đình nhỏ mang lại niềm vui cho em,
Và những đứa con có đôi mắt biếc và nụ cười tươi như em,
Yeah,
Thật lòng đó, anh cảm thấy tự hào về em,
Nhìn em xem, sự thành công biết bao người thèm,
Em đã thoát khỏi những con quỷ, anh vô tình san sớt,
Dù anh biết em không hề trách, và em đã nói là tình nguyện mang bớt,
Chúc em vui, chúc em có thật nhiều may mắn,
Chúc cho tất cả những ván bài chia đến tay em đều là cây thắng,
Và anh biết, những lời này đến từ anh là sự ngạc nhiên,
Nhưng có người nói hạnh phúc là khi ta đổi hận thù để nhận bình yên,
Because...

All my ex’s hate me,
And I hate them back,
And it’s all because of me,
And it’s all because of me.

Anh xin được dùng bài nhạc này gửi những người yêu cũ,
Rằng là,
Nếu em vẫn chưa thể yên ấm bên người yêu mới,
Anh cầu mong em sẽ tha thứ những điều ngược đãi mà anh đã làm,
Và, có 1 người tình nguyện vì em mà làm những điều mà anh chả ***,
Anh biết là anh đã hứa, không để em nước mắt dâng tràn,
Có lẽ tín dụng duy nhất anh có là khi nó nằm trong thẻ ngân hàng,
Anh ước 1 người nào đó, khiến con tim em hân hoan,
Và họ sẽ mang được đến cho em những điều mà em luôn xứng đáng,
Mong em khác, không đem những vết thương này theo mãi sau,
Anh xin lấy lại những câu nói khi cãi nhau,
Em cần biết, khi đau lòng thì anh mới buông lời ác,
Thật buồn cười, kẻ tổn thương lại muốn tổn thương người khác,
Right?
Nhưng mà, đó là Bảo, right?
Luôn làm lỗi, và lấy lý do là không hoàn hảo, right?
Khổng Tử nói là “với trả thù, luôn phải đào 2 mồ chôn”
Có lẽ vì thế gần đây anh có cảm giác mình không chỗ trốn,
Because...

Nếu ta quay về như lúc xưa ngày còn bên nhau
Thì chớ buông ra vài câu nói làm tim em thêm đau
Bước trên con đường nay đã không còn anh chung lối
Xoá đi thôi, nhẹ nhàng cuốn trôi', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 231, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (11, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/LoSayByeLaBye-LemeseChangg_iztK1IE6a.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Lỡ say Bye là Bye', N'Lemese, Changg', N'Nhạc trẻ', N'Hôm qua chia tay anh ta
Hôm qua anh không níu kéo, em không khóc mà
Hôm qua giấu hết nước mắt tổn thương vào trong
Hôm qua đã lỡ nói dối
Hôm qua tim chẳng thể ngừng đôi môi nói lời:
Anh ơi! Mình kết thúc thôi

[ĐK:]
Cứ thế xa nhau, anh không luyến tiếc gì
Sẽ mất bao lâu, em thôi ướt đẫm đôi mi
Nỗi nhớ kéo đến, xa rời anh có phải là sai
Hôm qua hai ta, lỡ say bye là bye..

[HOOK:]
Chết rồi! Mình đã nói chia tay người ta mất rồi!
Người đồng ý ngay khi em cất lời
Vậy là anh đã hết yêu em lâu lắm rồi...

[RAP:]
Vậy là chấm hết rồi đúng không
Vậy là tất cả đổ xuống sông
Thầm cầu mong đó là giấc mơ
Và tỉnh dậy anh vẫn ở kế bên
X2
Phải làm gì đây để tình yêu ấy
Chẳng còn vương vấn nơi đây
"Mình dừng lại thôi", nói đùa cho vui
Mà anh đồng ý quá vội chẳng cần suy nghĩ

I''m feel so crazy
Cause I still miss you babe
Anh là cành cây khô tôi là chiếc lá rơi khi cơn gió vụt qua

Một chút chết trong tim
Một nút thắt trong lòng
Rồi cứ thấy không yên
Và từng ngày tôi cứ ngóng trông
Điều đó đúng hay sai?
Liệu ta có nên dừng lại?
Rất nhiều câu hỏi đặt ra
Mà sự thật đã lỡ say bye là bye

[ĐK:]
Cứ thế xa nhau, anh không luyến tiếc gì
Sẽ mất bao lâu, em thôi ướt đẫm đôi mi
Nỗi nhớ kéo đến, xa rời anh có phải là sai
Hôm qua hai ta, lỡ say bye là bye..', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 215, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (12, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/Me-HoangDuyen_2-r44nigZ.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Mê', N'Hoàng Duyên', N'Nhạc trẻ', N'Verse 1
Có thể là ta có duyên gặp nhau
Chắc nên là tôi mới phải lòng anh
Người con trai có
Nụ cười duyên đó
Và đôi mắt cười là anh

Chẳng thể nào quên những khi gần anh
Có lẽ nào anh đã mê hoặc tôi
Chẳng thể từ chối
Chẳng thể nói dối
Rằng tôi thích anh rồiiii

Hù hú hu ôi chàng đẹp nhất thế gian
Người có phải thiên thần đến thế gian
Hù hú hu ôi chàng đẹp nhất thế gian
Chắc là, chắc là, chắc là thiên thần

Hù hú hu ôi chàng đẹp nhất thế gian
Người đến đây mê hoặc khắp thế gian
Hù hú hu ôi chàng đẹp nhất thế gian
Thích người bởi vì, bởi vì

Chorus
Bởi vì tôi đã mê anh rồi
Vẻ đẹp kia làm tôi u mê rồi
Thề rằng đó giờ chưa từng mê ai
Như anh

Ngoài kia có hàng trăm lối về
Mà nhà tôi chỉ một lối để về
Là lối vào tim của anh
Knock knock... mở cửa em vào

Verse 2
Anh gì ơi
Sao cứ lang thang, lang thang, lang thang
Lạc vào trái tim tôi hoài
Lại quên lối về
Người gì đâu sao mà kì cục quá
Đến nhà người ta mà chẳng chịu về

Anh gì ơi
Tôi lỡ thương anh, thương anh, thương anh,
Từng ngày đến cả ngây người
Mà chẳng hay gì
Lạy trời cao xin người tặng cho tôi
Người tôi hằng ao ước', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 219, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (13, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/NgayDauTien-DucPhuc_KHK5m12bV.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Ngày đầu tiên', N'Đức Phúc', N'Nhạc trẻ', N'Nhiều khi kiếm đâu cổ tích trên đời
Thật là điều may mắn nếu như có người
Cầm tay bước đi đến cuối con đường
bao giờ thì sẽ tới

Dẫu anh không là chàng hoàng tử
mà em đắm say
Chỉ là giản đơn thôi như hình hài anh lúc này
Anh vẫn muốn quỳ gối trước nàng công chúa
đẹp nhất đêm nay

Điều anh muốn là luôn thấy em cười
Chẳng cần phải lo lắng vì anh ở đây rồi
Để anh che chở em hết quãng đường
ngày sau nhé
Ngày đầu tiên cùng nhau sống suốt đời
Cùng nhìn về phía trước cầm tay mãi không rời
Và con tim cùng chung một nhịp khi ta có đôi

Điều anh mong thật ra chẳng xa xôi đâu
Chỉ cần ta cùng nhau đến khi bạc đầu
Dù mai sao nhiều điều làm ta lo âu

Yes or no
Yes or no

Dẫu anh không là chàng hoàng tử
mà em đắm say
Chỉ là giản đơn thôi như hình hài anh lúc này
Anh vẫn muốn quỳ gối trước nàng công chúa
đẹp nhất đêm nay

Điều anh muốn là luôn thấy em cười
Chẳng cần phải lo lắng vì anh ở đây rồi
Để anh che chở em hết quãng đường
ngày sau nhé
Ngày đầu tiên cùng nhau sống suốt đời
Cùng nhìn về phía trước cầm tay mãi không rời
Và con tim cùng chung một nhịp khi ta có đôi

Ngày hôm nay tuyệt vời nhất trên đời
Vì là ngày đầu tiên mà ta có nhau rồi
Và từ nay về sau sống chung trong một
THẾ GIỚI
Ngày đầu tiên cùng nhau sống suốt đời
Cùng nhìn về phía trước cầm tay mãi không rời
Và con tim cùng chung một nhịp khi ta có đôi', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 209, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (14, N'duydnt', N'https://ik.imagekit.io/nav26/MUZIC/song/ThichEmHoiNhieu-WrenEvans_vJx3Etn1N.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Thích em hơi nhiều', N'Wren Evans', N'Nhạc trẻ', N'Biết là mình thích nhau
Còn chuyện xa hơn thôi để tính sau
Vờn nhau thôi không đúng đâu
Dân chơi thì chơi là phải trúng
Còn anh chỉ muốn chậm thôi, không cần quá liều
Oh anh chưa muốn đâm đầu vậy đâu
Anh chưa thiết tha nghĩ tới chuyện sau này
Dù ba mẹ chờ mong, em biết không?
Họ muốn có cháu bồng
Chỉ cần ngồi với em chút thôi, được không?
Nhìn vào đôi mắt em thấy sao? Một thế giới màu hồng
Dù môi đang cách nhau, cách nhau một chút
Nhưng mà vì anh chưa muốn phiêu, muốn phiêu
Dù thích em hơi nhiều

"Alo"
"Baby anh đang đâu thế?"
Anh ngồi Xofa ở Tống Duy Tân, ngay cạnh Puku, oke?
Với mấy đồng bọn nó rủ rê
Qua nhà thằng Minh Khai không thật
Cơ mà có hẹn với em anh bảo tối nay tao đi sinh nhật, thế nhá
Nói thật anh đang bối rối khi nghĩ về ai
Công việc dang dở chắc anh phải chia ông Hiếu làm hai
Thích em hơi nhiều nên anh phải nghĩ sao nó vừa tai
Tại vì most of the time
Em ở trong tâm trí anh hết thời gian

Chỉ cần ngồi với em chút thôi, được không?
Nhìn vào đôi mắt em thấy sao? Một thế giới màu hồng
Dù môi đang cách nhau, cách nhau một chút
Nhưng mà vì anh chưa muốn phiêu, muốn phiêu
Dù thích em hơi nhiều
Baby anh muốn thấy em trong tương lai gần', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 172, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (15, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/ThichQuaRoiNa-TlinhTrungTranWxrdieNgerPacman_Qp6TwEVaM.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Thích Quá Rồi Nà', N'Tlinh, Trung Trần, Wxrdie, V.A', N'Nhạc trẻ', N'Em nói đừng tin vào tình yêu à?
Nhưng khi trông thấy em con tim anh loạn nhịp
Anh cho rằng lẽ nào mình siêu thật
Bởi vì em nói nhớ anh mỗi khi màn đêm buông xuống thành phố này, tan tầm cuối ngày

I''m on my way em đang ở đâu anh tới ngay
Baby call vào số này gửi anh location
Ghé qua nhà em, môi chạm tai yehhh
I''ve got something to say

Đừng làm cho em bối rối nữa đi vì mỗi khi gần anh em như ngừng thở
Và nếu như anh đang cố tình làm em mê anh
Thì anh đừng mơ vì em ngại em không nói đâu
Nên em mong anh tự biết bàn tay này muốn chạm vào ai rồi

Biết trong đầu này chỉ có ai đó thôi
Biết đôi má của em đang ửng hồng rồi
Và biết trái tim này lỡ mất vài nhịp rồi
Biết ngại ngùng khi ánh mắt chạm nhau rùi

Biết hơi thở như dừng lại vì có ai
Đừng hơi gần so với cho phép nhớ
Và biết em lỡ thích anh hơi nhiều quá rùi nà
Uuuuu hay lại để mai ta gặp nhau

Lại để lời yêu ra đằng sau
Để cho mọi thứ như chưa bắt đầu (damn girl)
Can''t put u out of my mind you pretty girl u got a style
Bảo rằng anh yêu em

And drop the mic
Chẳng cần nói nhiều đâu, anh biết rằng tâm hồn em mang chiều sâu
Oi anh ở gần quá em không *** đâu
Oh ngại quá đi but i like it tho

Bởi vì anh chưa biết liệu mình nên cưa tiếp
Hay liều một phen chẳng may có được em
Em chỉ đợi một câu nói thôi, rằng anh đã
Biết bàn tay này muốn chạm vào nhau rồi

Biết trong đầu này chỉ thấy có nhau thôi
Biết đôi má cả hai đã ửng hồng rồi
Và biết trái tim này đập chung nhịp rồi
Biết ngại ngùng khi ánh mắt chạm nhau rùi

Biết mũi tên thần tình yêu đã bắn đi
Trúng hay hụt em chắc cupid không hụt đâu
Biết ta lỡ thích nhau hơi nhiều quá rùi nà', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 177, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (16, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/ThisWay-CARA_l7kziAan8.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'This way', N'Cara', N'Nhạc trẻ', N'1. Lần đầu tiên gặp nhau mà nhìn nhau hơi bị lâu
Lặng nhìn đôi mắt bồ câu, làm nhịp tim hơi bị đau
Từ lâu không gặp ai, mà gặp được anh đẹp dzai
Biết ngay mà
Mê chữ ê kéo dài

Thật chẳng biết từ đâu, hai cục nam châm gặp nhau
Một điều gì rất đậm sâu, buộc chặt hai ta vào nhau
Chỉ cần người nói một câu, vậy là xong luôn oh no
Em trót yêu rồi

[ĐK:]
Sao anh cứ đi loanh quanh?
Sao anh cứ vấn vương ở trong đầu em mãi thế?
Hình như anh trót quên lối về
Cho em nắm tay anh để khỏi lạc đường anh nhé
This way, this way
Lối này vào tim em đấy
This way, this way
Lối này vào tim em này

2. Trời sinh voi, trời sinh cỏ
Em là cà rốt, anh là con thỏ
Oải hương màu xanh, hoa hồng màu đỏ
I say meow, you say awww
Cho em hỏi nhỏ này
Anh có bị cận thị không vậy?
Em đã bật đèn xanh rồi đấy
Sao anh cứ đạp phanh mãi vậy?

Dự báo thời tiết nói hôm nay sẽ có nắng ở trong lòng em
Thì ra sáng nay anh đã mang mặt trời đến bên thật dịu êm
Nơi thiên đàng đâu có xa xôi
Vì em đã tìm thấy anh trên đời (Oh baby)

[ĐK:]
Sao anh cứ đi loanh quanh?
Sao anh cứ vấn vương ở trong đầu em mãi thế?
Hình như anh trót quên lối về
Cho em nắm tay anh để khỏi lạc đường anh nhé

This way, this way
Lối này vào tim em đấy
This way, this way
Lối này vào tim em này

Chàng trai xinh đẹp ơi, cuộc đời đâu dễ tìm nhau
Mình gặp được nhau là duyên, một sự thật quá hiển nhiên
Cầm tay anh thật lâu, và nhẹ nhàng nói một câu
“Anh ơi lối này”

[ĐK:]
Sao anh cứ đi loanh quanh?
Sao anh cứ vấn vương ở trong đầu em mãi thế?
Hình như anh trót quên lối về
Cho em nắm tay anh để khỏi lạc đường anh nhé

This way, this way
Lối này vào tim em đấy
This way, this way
Lối này vào tim em này
This way, this way
Lối này vào tim em đấy
This way, this way
Lối này vào tim em này', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 197, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (17, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/ThoiQuenHoangDung-GDuckymp3_-donhnDbQ.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Thói quen', N'Hoàng Dũng, GDucky', N'Nhạc trẻ', N'1. Dù rằng anh cũng biết
Phải tự dặn mình để tốt lên theo từng ngày
Anh vẫn có đôi ít thói *** không đáng khen trong cuộc sống
Vì ngày còn dài mênh mông
Vì sợ rằng tâm trí anh không đủ rộng
Nên đôi khi anh vẫn cố cho anh thêm một lần không để tâm

Anh vẫn thường tìm đến một điếu thuốc khi anh cần phải nhớ
Uống thật say vào đêm anh cần phải quên
Thức thật khuya để thấy yên bình thêm
Thật nhiều những điều dẫu là không nên, anh chưa buồn thay đổi
Cố tình biếng lười, hay đi về sớm tối
Và vẫn còn yêu em cũng thế thôi

[ĐK:]
Vì lúc em xa anh và bước đến chân trời mới
Thì đáng ra anh nên ngừng thương nhớ em rất lâu rồi
Tìm đến những thói *** dù ngốc nghếch vẫn chẳng muốn xa rời
Chỉ để lấp đầy hết trống rỗng bên trong anh mà thôi

2. Và rồi nhiều ngày không em
Anh vẫn chưa bỏ được những thói *** mà em cho là xấu
Liệu em có trách anh không nếu ta tình cờ chạm mặt nhau?
Anh như một người cô đơn tham lam
Có đôi tai muốn lắng nghe những lời hứa mãi về sau
Cùng đôi mắt muốn thấy em vẫn thân thuộc như ngày ta bắt đầu

Em vẫn thường buộc tóc thật xinh đợi trông những lần đưa đón
Vẫn cười tươi vì những câu đùa cỏn con
Thức thật khuya chờ dòng tin nhắn chúc em ngủ ngon
Thật nhiều những điều dẫu mình xa nhau nhưng chưa hề thay đổi
Em giờ vẫn yêu say đắm một người
Chỉ là giờ người ấy chẳng còn là anh nữa thôi.

[ĐK:]
Là lúc em xa anh và bước đến chân trời mới
Thì đáng ra anh nên ngừng thương nhớ em rất lâu rồi
Tìm đến những thói *** dù ngốc nghếch vẫn chẳng muốn xa rời
Chỉ để lấp đầy hết trống rỗng bên trong anh mà thôi

Tadadi’da
Tadadi’da
Tadadida dada
Ai đó sẽ đến đón đưa em đi cùng những thói *** có anh
Tadadi’da
Những thói *** sẽ đón đưa anh đi về
Tadadida
Tadadida', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 271, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (18, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/31072-DuonggNauWn_RDw4_YHOH.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'3107-2', N'Duongg, Nâu, W/n', N'Nhạc trẻ', N'Đến bao giờ mới có thể quên đi những câu chuyện mà ta đã trao
Đến bao giờ mới có thể yêu một người đến sau
Đến bao giờ mới có thể quên đi những kỉ niệm mà ta đã qua
Và nếu hôm nay là ngày mà chúng ta đã rời xa.

Chỉ cần ai đó cạnh bên dừng lại và níu lấy em chỉ một phút giây
Dù là có đúng hay sai vẫn cứ yêu thêm một lần chẳng cần nghi ngại
Ngày dài vẫn thế cứ thế trôi hoài, lạc mất nhau sao mình còn tìm mãi
Nơi anh đến sẽ là một nơi chẳng còn có em.

[Rap]
Anh có bàn tay này nắm tất cả nhưng không thể nắm được tay em
Anh có niềm tin là thứ duy nhất ở lại cuối cùng.
Anh đã từng có vầng trời của mình nhưng trời chỉ toàn đám mây đen
Cũng chẳng quán *** đường mình thường ghé vào dịp cuối tuần.

Anh thấy không ổn nhưng vẫn cố tỏ ra không sao
Điều tệ nhất là em đi mất chỉ còn đôi tất cuối cùng trong bao
Mưa cũng chẳng giúp anh được gì ngoài việc làm ướt đi áo sơ mi
Thắt cà vạt chỉnh chu lại áo nhưng cuối cùng vẫn bị em lơ đi.

Đến bao giờ mới có thể quên đi những câu chuyện mà ta đã trao
Đến bao giờ mới có thể yêu một người đến sau
Đến bao giờ mới có thể quên đi những kỉ niệm mà ta đã qua
Và nếu hôm nay là ngày mà chúng ta đã rời xa.

Chỉ cần ai đó cạnh bên dừng lại và níu lấy em chỉ một phút giây
Dù là có đúng hay sai vẫn cứ yêu thêm một lần chẳng cần nghi ngại
Ngày dài vẫn thế cứ thế trôi hoài, lạc mất nhau sao mình còn tìm mãi
Nơi anh đến sẽ là một nơi chẳng còn có em.

Giờ giọt nước mắt cứ thế lăn dài ngày tháng trôi đi chẳng thể níu ai
Rồi ngày mai đến ra sao khi cứ trông mong một người sẽ còn ở lại
Ngày hạ năm ấy còn nhớ những gì và chúng ta đã là hai thế giới
Thế giới ấy bây giờ chẳng còn điều gì giống ai.', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 211, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (19, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/AnhKhongThaThu-DinhDung_djucbaO7u.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Anh không tha thứ', N'Đình Dũng', N'Nhạc trẻ', N'1. Bờ mi ấy xin đừng ngấn lệ như ngày em bước đi
Giờ ôm than vãn cũng ích gì người bận tâm chi
Giờ nơi phương xa đó nhung gấm quanh mình
Người có nhớ không nghĩa tình?

Khó khăn còn thương nhau mãi đâu sao giờ u sầu
Chỉ mong em sẽ chọn đúng người thật lòng thương lấy em
Bởi em đau anh cũng rất buồn vì mình từng yêu thương
Và xin em hãy nhớ lấy tháng năm này
Mình đã có nhau như vậy
Lỡ sau này ta trông thấy nhau xin đừng ngoảnh đầu

[ĐK:]
Cả cuộc đời này anh không tha thứ cho bản thân mình vì đánh mất em
Một người đàn ông nhưng đang than khóc như một đứa trẻ
Muộn phiền nào hơn khi anh trông thấy
Thế giới của mình sánh bước bên ai
Vì sao người lại đang tâm nỡ xé tim anh làm hai

2. Chỉ mong em sẽ chọn đúng người thật lòng thương lấy em
Bởi em đau anh cũng rất buồn vì mình từng yêu thương
Và xin em hãy nhớ lấy tháng năm này
Mình đã có nhau như vậy
Lỡ sau này ta trông thấy nhau xin đừng ngoảnh đầu

[ĐK:]
Cả cuộc đời này anh không tha thứ cho bản thân mình vì đánh mất em
Một người đàn ông nhưng đang than khóc như một đứa trẻ
Muộn phiền nào hơn khi anh trông thấy
Thế giới của mình sánh bước bên ai
Vì sao người lại đang tâm nỡ xé tim anh làm hai

[ĐK:]
Cả cuộc đời này anh không tha thứ cho bản thân mình vì đánh mất em
Một người đàn ông nhưng đang than khóc như một đứa trẻ
Muộn phiền nào hơn khi anh trông thấy
Thế giới của mình sánh bước bên ai
Vì sao người lại đang tâm nỡ xé tim anh làm hai
Vì sao người lại đang tâm nỡ xé tim anh làm hai', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 274, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (20, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/Breakfast-DHGDuckyMinh_DeEUmZTDC.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Breakfast', N'DH, GDucky, Minh', N'Rap Việt', N'Order pizza, café trung, thêm ly machi a tồ
Cappucino, cho bình minh hôn đôi môi em vị si rô
Còn ly thuy tinh, chai rượu rang vang định vị làm sao được xa gần
Eiffel trên đỉnh, kem bao xung quanh Macarons vị hoa hồng

Bày ratatouille, chắc anh trở thành linguini
Ngọt vương vai em, Gelato mùi hương vani (hương vani)
Một chút hạt tiêu, trải đều lên carbonara (trải lên ca bô na rá á á)
Ngả lưng xuống con sóng này mang ta ra xa

Mặt trăng đêm nay tựa như pink star Diamond
Tìm em đêm nay, đêm nay, ta có những thứ ngày mai cần
Air king trên tay anh (rolex), và thời gian anh dành cho em
Weed a little bit, mặt trăng này đang dần to lên, sunshine

Môi em chạm miệng ly, hải âu tản mản cản biển liệng đi
I’m Jerry, babe I share my cheese
Miệng anh giữ nắp champagne, tai đang đuổi bắt âm thanh
I’m feeling crazy, give me a beat

Please give me a beat when I’m making breakfast
Please give me a beat when I’m making breakfast
Please give me a beat when I’m making breakfast
Baby please give me a beat when I’m making breakfast

Muốn làm cho em 1 bữa sáng thật healthy và balance
Một bài thể dục ở giữa háng, tập cùng em vài ba lần
A sausage with two omelets and some bacon
Lại gần giường em, hôn đôi môi em và châm cho em 1 vacant

Này ratatouille chắc anh trở thành linguini
Anh đưa em lên sofa, xem phim như ở CGV
I’m finding out her spot, they call me the Shinichi
Em ơi lên đây đu đưa đi

Từ Tây Ban Nha sang Tunisia
Lênh đênh trên du thuyền mà anh thả đâu đây
I’m creaming over you, vì em là quả dâu tây
On a Dubai trip, mua cho em thật nhiều vàng

Baby please give me a beat, để đêm nay anh được chiều nàng
Please give me a beat when I’m making breakfast
Please give me a beat when I’m making breakfast
Please give me a beat when I’m making breakfast

Baby please give me a beat when I’m making breakfast
Hoàng hôn tan ca, em, vội ngả lưng ra sofa
Chivas 12, thêm 3 lần coca cola pha soda
Chân anh vội bước, qua cánh cửa, gặp mặt em aloha

Book về Hawaii, với em không quan tâm chi vài ba đô la
Anh không cứng cũng chẳng mềm như linguini
Anh không phải là neard, cũng chẳng gangster như Eazy-E, và
Vài mili men, trước màn hình và DVD

Em muốn sao cũng xong hết vì anh không phải LGBT
Đánh thức thính giác em là phô mai, anh luôn tìm như Jerry (Jerry)
Tỏa mùi hương ngây ngất, em như là blue berry (blue berry)
Netflix and smoke, cũng biết em là mê phim

Giữ tỉnh táo để tỉnh táo, mà không cần cafeine
Please give me a beat when I’m making breakfast
Please give me a beat when I’m making breakfast
Please give me a beat when I’m making breakfast

Baby please give me a beat when I’m making breakfast
Chiều nay đánh con gì, suy nghĩ nào, suy nghĩ nào
Chiều nay đánh con này, thế *** nào, nó lại về con kia hahaha', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 206, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (21, N'duynk', N'https://ik.imagekit.io/nav26/MUZIC/song/CoGaiVang-HuyRTungViu_E5F-rvd96.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Cô gái vàng', N'HuyR, Tùng Viu', N'Nhạc trẻ', N'Son môi em đánh, mới order ở bên Tây
Bao anh điêu đứng, lúc em tải hình lên face
Áo quần thuê thiết kế riêng
Em ngồi siêu xe rất duyên
Ba em bác sĩ, má em làm chủ công ty
Mỗi cô con gái, nên nuông chiều nhiều hơn ai
Chỉ cần em nói thích gì
Mai là có, không nghĩ suy
Vậy mà chẳng thể hiểu kiểu gì em không thích anh
Một người vừa hiền lành, học giỏi, lại tính toán nhanh
4-3 bằng 2
Yêu anh không thể sai
Bao chàng trai ngoài kia làm sao sánh với anh được

[ĐK:]
Nhà giàu lại còn xinh, em là cô gái vàng
Mọi người gặp đều khen anh đẹp trai nhất làng
Mình vừa hợp một đôi, như giành cho nhau ấy
Chẳng hiểu kiểu gì luôn, cứ giống như phim vậy
Thường ngày kể về em, ba mẹ anh hết lời
Giờ mà về làm dâu, chắc là thương suốt đời
Vì nụ cười của em anh mộng mơ sớm tối
Vừa gặp lần đầu tiên anh đã say mê rồi

[RAP:]
Bao đời nhà anh úp bát vào chạn bây giờ anh muốn được úp vào em
Mà nếu em thấy cuộc đời còn nhạt để anh dùng nhạc cùng muối xào lên
Anh biết em khổ từ nhỏ ngày nào cũng phải nghĩ cách tiêu tiền
Khi em phải sống trong một gia đình có một năng lực tài chính siêu nhiên
Nghe thoang thoáng ba em chủ tịch
Họp cả tháng vẫn không đủ lịch
Mẹ làm chủ một chuỗi nhà hàng mở chục chi nhánh ở tận bên Sing
Thời gian bên em thì ít vì có những hôm phải bận quên mình
Em đi siêu xe em ở biệt thự cớ sao em vẫn lận đận duyên tình
Huy bạn anh thì on da beat
Anh nhìn em thì yêu ra phết
Ba mẹ anh thì lo ra Tết
Sợi dây tơ hồng của đôi ta kết
Không còn phải một mình mỗi ngày 2 chục nghìn lẻ năm trăm
Hai mình ăn mẹt bún đậu ngập mặt luôn để cuộc tình mình sẽ trăm năm
Có lẽ những chân thành em lại nghĩ bông đùa
Đẹp trai nhưng anh không dối đâu (dối đâu)
Anh không giống như là bao chàng trai em thường thấy bấy lâu
Bé đến lớn bây giờ anh chỉ yêu một người
Mọi người ở xung quanh chứng kiến giúp anh
Chỉ có mỗi em là, anh thương nhớ thôi mà
Nắng sớm đến đến trăng tà a ì a i á i a i à', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 228, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (22, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/DeMiNoiChoMaNghe-HoangThuyLinh_gfbAHuG9oOi.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Để Mị nói cho mà nghe', N'Hoàng Thùy Linh', N'Nhạc trẻ', N'Để Mị nói cho mà nghe
Tâm hồn này chẳng để lặng lẽ
Thương cha xót mẹ
Thôi thì mặc phận đời mình chơi vơi

Để Mị nói cho mà nghe
Hết năm nay Mị vẫn còn trẻ
Xuân đương tới rồi
Nên Mị cũng muốn đi chơi

[Chorus]
Này là mình đi theo giấc mơ sớm mai gọi mời
Nơi vòng tay ấm êm chẳng rời
Hoa ban trắng nở đầy con bản nọ
Hương sắc còn chưa phai

Đời mình đâu có mấy vui cớ sao lại buồn
Biết ngày mai trắng đen hay tròn vuông
Em không bắt quả pao rơi rồi
Tiếc không một đời đơn côi

[Rap]
Mị còn trẻ Mị còn muốn đi chơi
Thanh xuân sao lại phải nghỉ ngơi
Hoa ban trắng trên nương chớm nở
Đẹp như tâm hồn em còn ngây thơ

Em làm gì đã có người yêu
Em còn đang sợ ế đây này
Vậy tại sao quả pao không năm trên tay
Để bao trai làng chìm đắm trong mê say

Mùa xuân này, Mị muốn xúng xính trong váy hoa
Không đi làm sao biết ngoài kia một mai là sương hay nắng toả
Cơ hội này Mị sẽ nắm lấy, Mị chẳng cần một ai dắt tay
Và hết hôm nay, Mị sẽ chuồn khỏi nơi đây!
[Verse]
Để Mị nói cho mà nghe
Tâm hồn này chẳng để lặng lẽ
Thương cha xót mẹ
Thôi thì mặc phận đời mình chơi vơi

Để Mị nói cho mà nghe
Hết năm nay Mị vẫn còn trẻ
Xuân đương tới rồi
Nên Mị cũng muốn đi chơi

[Chorus]
Này là mình đi theo giấc mơ sớm mai gọi mời
Nơi vòng tay ấm êm chẳng rời
Hoa ban trắng nở đầy con bản nọ
Hương sắc còn chưa phai

Đời mình đâu có mấy vui cớ sao lại buồn
Biết ngày mai trắng đen hay tròn vuông
Em không bắt quả pao rơi rồi
Tiếc không một đời đơn côi', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 194, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (23, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/DieuAnhBiet-ChiDan_bRArU1f0x.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Điều anh biết', N'Chi Dân', N'Nhạc trẻ', N'Anh không biết bao nhiêu sao trên trời
Anh không biết cuộc đời mai ra sao
Dù gian lao dù ra sao thì anh vẫn luôn có
Có một người luôn bên cạnh anh mãi thôi
Anh không biết yêu em sao cho vừa
Anh không biết ngọt ngào hay trăng sao
Tình yêu anh dù không mấy lời
Nhưng lòng anh biết ý nghĩa cuộc đời này là khi có em
Và em ơi điều anh biết là
Mỗi khi em cười là bao nhiêu phiền lo trong đời biến tan
Và em ơi điều anh biết là
Nỗi nhớ tơi bời lúc em rời xa chốn đây em giận anh
Và em ơi điều anh biết là
Lắm khi đau lòng vì anh vô tình cho em buồn xót xa
Và em ơi điều anh biết là Không cần câu hứa đã yêu hôm nay yêu nhiều hơn lúc xưa
Anh trông thấy bao bông hoa trên đời
Anh không thấy người nào xinh hơn em
Vì anh yêu trọn đôi mắt này
Yêu trọn tâm trí chỉ xin một đời này được che chở em

Anh không biết bao nhiêu sao trên trời
Anh không biết cuộc đời mai ra sao
Dù gian lao dù ra sao thì anh vẫn luôn có
Có một người luôn bên cạnh anh mãi thôi
Anh không biết yêu em sao cho vừa
Anh không biết ngọt ngào hay trăng sao
Tình yêu anh dù không mấy lời
Nhưng lòng anh biết ý nghĩa cuộc đời này là khi có em
Và em ơi điều anh biết là
Mỗi khi em cười là bao nhiêu phiền lo trong đời biến tan
Và em ơi điều anh biết là
Nỗi nhớ tơi bời lúc em rời xa chốn đây em giận anh
Và em ơi điều anh biết là
Lắm khi đau lòng vì anh vô tình cho em buồn xót xa
Và em ơi điều anh biết là
Không cần câu hứa đã yêu hôm nay yêu nhiều hơn lúc xưa

Và em ơi điều anh biết là
Mỗi khi em cười là bao nhiêu phiền lo trong đời biến tan
Và em ơi điều anh biết là
Nỗi nhớ tơi bời lúc em rời xa chốn đây em giận anh
Và em ơi điều anh biết là
Lắm khi đau lòng vì anh vô tình cho em buồn xót xa
Và em ơi điều anh biết là
Không cần câu hứa đã yêu hôm nay yêu nhiều hơn lúc xưa
Anh trông thấy bao bông hoa trên đời
Anh không thấy người nào xinh hơn em
Vì anh yêu trọn đôi mắt này
Yêu trọn tâm trí chỉ xin một đời này được che chở em.', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 315, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (24, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/DuChoMaiVeSau-buitruonglinh_TwY6y9D5W.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Dù cho mai về sau', N'Bùi Trường Linh', N'Nhạc trẻ', N'Bầu trời đêm không mây không sao trăng treo trên cao khi lòng anh vẫn nhớ nhung em nhiều
Anh làm sao có thể ngừng suy nghĩ đến đôi môi em dù chỉ một giây
Mặc cho ta đi bên nhau bao lâu
Em đâu hay anh cần bao câu nói anh yêu em chỉ để anh sẽ một lần nhìn thấy trái tim em đang rung động xiết bao

Dù lời nói có là gió bay, anh vẫn mong sau này chúng ta trở thành của nhau
Mệt thì cứ ngoái lại phía sau anh vẫn luôn đây mà
Dù thời gian không chịu đứng yên để cho chính anh không còn ngẩn ngơ cũng thôi mơ mộng
Thì anh vẫn luôn dành những câu ca trong lòng anh cho người mãi thôi

Dù cho mai về sau
Mình không bên cạnh nhau
Làm tim anh quặn đau
Anh trông ngóng bao nhiêu lâu
Dù vương vấn u sầu
Mùa thu có phai màu
Anh vẫn muốn yêu em

Dù cho muôn trùng phương
Còn bao nhiêu lời thương
Dù mênh mông đại dương
Phai đi sắc hương mơ màng
Anh vẫn yêu mình em thôi đấy em ơi đừng để tình anh dở dang

Anh vẫn yêu mình em thôi đấy yêu em mà chẳng một lời thở than

(coda)
Dù lời nói có là gió bay
Dù ngày tháng có còn đổi thay
Thì anh vẫn mãi muốn nắm đôi bàn tay dắt theo hy vọng đong đầy





(Rap)
(Ôi giờ ơi) Rất nhiều câu trả lời chỉ để em nhớ được điều này nữa thôi
Anh vẫn luôn muốn ở cạnh em bất kể dù cho nhiều ngày nữa trôi -
Hồi đầu giả vờ ngơ ngẩn một mình dưới tán lá cây mà ngắm trời
Ai biết *** được mấy tháng mới phát hiện em cũng khá nhây và lắm - lời (Nói nhiều thật đấy)

Em thì vẫn đáng yêu trừ những lúc dở chứng cáu gắt mắng đủ điều
Đáng yêu từ cái dáng điệu dựa vào anh mỗi khi trời tắt nắng buổi chiều
(Haizz) - Biết là làm dược sĩ nên khó tính hay nhắc nhắn nhủ nhiều
Như là ôm quả bom nổ chậm vì anh mà yêu em thì phải chắc chắn đủ liều

Cho thời gian đưa lối
Anh không mong điều gì xa xôi
Yêu chỉ mình em thôi khiến anh thức thao cứ mỗi đêm về
Em lại xinh như thế
Sao mà không làm cho anh mê đắm như trong giấc mơ
Khi lắng nghe tiếng yêu bấy lâu anh mong chờ
Em nhẹ ngân câu hát
Theo điệu đàn anh feel
Ta đi bên cạnh nhau bao nhiêu thế nhưng vẫn luôn nhớ em hơn nhiều

Anh mong dù mai này có giận - thì mình cũng đừng có cãi lời nhau
Khi người anh thương chỉ luôn có một - dù cho mãi về sau', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 214, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (25, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/GacLaiAuLo-DaLABMiuLe_9H3SvkLD9.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Gác lại âu lo', N'Da Lab, Miu Lê', N'Nhạc trẻ', N'Anh đi lạc trong sóng gió cuộc đời
Nào biết đâu sớm mai liệu bình yên có tới?
Âu lo chạy theo những ánh sao đêm
Ngày cứ trôi chớp mắt thành phố đã sáng đèn
Ta cứ lặng lẽ chạy thật mau, yêu thương chẳng nói kịp thành câu...
Biết đâu liệu mai còn thấy nhau?
Thức giấc để anh còn được thấy ánh mắt của em nhẹ nhìn anh, đôi tay này sẽ không xa rời

Tạm gác hết những âu lo lại, cùng anh bước trên con đường
Ta sẽ không quay đầu để rồi phải tiếc nuối những chuyện cũ đã qua
Giữ trái tim luôn yên bình và quên hết những ưu phiền vấn vương
Cuộc đời này được bao lần nói yêu
Anh biết nơi để quay về, em biết nơi phải đi
Anh biết chỗ trú chân dọc đường để tránh cơn mưa hạ đến mỗi chiều
Ta biết trao nhau ân cần, biết mỗi khi vui buồn có nhau
Thời gian để ta trưởng thành với nhau


Nhảy với anh đến khi đôi chân rã rời
Hát anh nghe những câu ca từ ngày xưa cũ
Thì thầm khẽ anh nghe em vẫn còn bao niềm mơ
Ôm lấy anh nghe mưa đầu mùa ghé chơi
Một giây không thấy nhau như một đời cô đơn quá
Trời mù mây bỗng nhiên ngát xanh khi em khẽ cười
Một ngày anh biết hết nguyên do của những yên vui trong đời
Ngày mà duyên kiếp kia đưa ta gần lại với nhau

Tạm Gác hết những âu lo lại, cùng anh bước trên con đường
Ta sẽ không quay đầu để rồi phải tiếc nuối những chuyện cũ đã qua
Giữ trái tim luôn yên bình và quên hết những ưu phiền vấn vương
Cuộc đời này được bao lần nói yêu
Anh biết nơi để quay về, em biết nơi phải đi
Anh biết chỗ trú chân dọc đường để tránh cơn mưa hạ đến mỗi chiều
Ta biết trao nhau ân cần, biết mỗi khi vui buồn có nhau
Thời gian để ta trưởng thành với nhau

Bờ vai anh rộng đủ để che chở cho emmmmm
Was a boy now a man cho emmm
Từng đi lạc ở trong thế giới điên zồ ngoài kia
và tình yêu em trao anh ngày ấy đã mang anh về bên emmmm
Yêu em như a Fat kid loves cake
Nhắm mắt cảm nhận tình yêu tan dịu ngọt trên môi khi em hôn môi anh đây, yo
Không phải happy ending, mỗi bình minh ta viết thêm trang mới, nối dài câu chuyện mình

Như trong mơ nơi xa kia xanh biếc xanh biếc
Thiên đàng bên em nơi đây anh biết anh biết
Bóng đêm đã qua yên bình có thêm chúng ta nghe lòng đàn từng câu ca
Cuộc đời này chẳng hề hối tiếc
Những tháng năm ta đi cùng nhau

Anh biết em luôn ở đó nơi anh thuộc về
Tạm Gác hết những âu lo lại, cùng anh bước trên con đường
Ta sẽ không quay đầu để rồi phải tiếc nuối những chuyện cũ đã qua
Giữ trái tim luôn yên bình và quên hết những ưu phiền vấn vương
Cuộc đời này được bao lần nói yêu
Anh biết nơi để quay về, em biết nơi phải đi
Anh biết chỗ trú chân dọc đường để tránh cơn mưa hạ đến mỗi chiều
Ta biết trao nhau ân cần, biết mỗi khi vui buồn có nhau
Thời gian để ta trưởng thành với nhau', N'Bài này chill phết', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 283, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (26, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/GoiMua-TrungQuanIdol_BZQoYcKRQ.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg  ', N'Gọi mưa', N'Trung Quân Idol', N'Nhạc trẻ', N'[Verse 1]
Mưa ơi rơi nhanh cho chiều giông tố đến tơi bời
Để ta ngồi lại nơi đây...
Nhưng em buông lơi chẳng muốn ôm chặt đôi tay này
Nhìn em rời bước xóa hết yêu thương ngọt ngào bên nhau
Đã một thời anh tin... sẽ không tàn
Còn chiếc hôn trao cho anh vội vàng
Em mang mọi thứ xung quanh anh... tan vào mưa

[Hook]
Nếu để cho ai đó cần em hơn anh
Anh sẽ ra đi... anh sẽ đi
Vì anh... nào đâu muốn em phải lựa chọn
Con tim ơi ngủ ngoan
Nhủ lòng mình mau trói bao yêu thương vùng lên mỗi đêm
Mà em nào biết rời xa nơi anh là bão tố

[Chorus]
Hứa yêu nhau đậm sâu này bước qua nhau rồi mong những gì
Cho anh lúc đầu, em nói đi
Mưa hãy cuốn đi, trôi mờ hết đi từng màu buồn yêu
Có khi nào ngủ mơ em nhớ trang giấy mà ôm lấy anh
Nhưng anh vẫn đang thét lên, mà em nào biết
Ngoài vòng tay anh là bão tố

[Verse 2]
Mong em yên vui bên tình yêu mới... là dối lòng
Vì vòng tay đó từng của anh
Nhưng trong cơn mưa em nói điều làm anh không ngờ...
Nhìn em rời bước xóa hết yêu thương ngọt ngào bên nhau
Đã một thời anh tin sẽ không tàn
Còn chiếc hôn trao cho anh vội vàng
Em mang mọi thứ xung quanh anh... tan vào mưa

[Hook]
Sự thật này... đâu như anh ước mơ đâu như anh đã đợi chờ
Tổn thương hôm nay anh giấu... em thấu mấy phần...

[Chorus]
Mưa ơi rơi nhanh
Mà em nào biết, ngoài vòng tay anh là bão tố
Mưa ơi rơi nhanh', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 350, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (27, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/HayTraoChoAnh-SonTungMTPSnoopDogg_UWUTo13w_.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Hãy trao cho anh', N'Sơn Tùng M-TP, Snoop Dogg', N'Nhạc trẻ', N'Bóng ai đó nhẹ nhàng vụt qua nơi đây
Quyến rũ ngây ngất loạn nhịp làm tim mê say
Cuốn lấy áng mây theo cơn sóng xô dập dìu
Nụ cười ngọt ngào cho ta tan vào phút giây miên man quên hết con đường về eh
Chẳng thể tìm thấy lối về ehhhhh
Điệu nhạc hòa quyện trong ánh mắt đôi môi
Dẫn lối những bối rối rung động khẽ lên ngôi

Chạm nhau mang vô vàn
Đắm đuối vấn vương dâng tràn
Lấp kín chốn nhân gian
Làn gió hóa sắc hương mơ màng
Một giây ngang qua đời
Cất tiếng nói không nên lời
Ấm áp đến trao tay ngàn sao trời lòng càng thêm chơi vơi
Dịu êm không gian bừng sáng đánh thức muôn hoa mừng
Quấn quít hát ngân nga từng chút níu bước chân em dừng
Bao ý thơ tương tư ngẩn ngơ
Lưu dấu nơi mê cung đẹp thẫn thờ

Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh thứ anh đang mong chờ
Hãy trao cho anh
Hãy trao cho anh
Hãy mau làm điều ta muốn vào khoảnh khắc này đê
Hãy trao cho anh
Hãy trao cho anh
Hãy trao anh trao cho anh đi những yêu thương nồng cháy
Trao anh ái ân nguyên vẹn đong đầy

Looking at my Gucci is about that time
We can smoke a blunt and pop a bottle of wine
Now get yourself together and be ready by nine
Cuz we gon’ do some things that will shatter your spine
Come one, undone, Snoop Dogg, Son Tung
Long Beach is the city that I come from
So if you want some, get some
Better enough take some, take some


Chạm nhau mang vô vàn
Đắm đuối vấn vương dâng tràn
Lấp kín chốn nhân gian làn
Gió hóa sắc hương mơ màng
Một giây ngang qua đời
Cất tiếng nói không nên lời
Ấm áp đến trao tay ngàn sao trời lòng càng thêm chơi vơi
Dịu êm không gian bừng sáng đánh thức muôn hoa mừng
Quấn quít hát ngân nga từng chút níu bước chân em dừng
Bao ý thơ tương tư ngẩn ngơ
Lưu dấu nơi mê cung đẹp thẫn thờ

Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh thứ anh đang mong chờ
Hãy trao cho anh
Hãy trao cho anh
Hãy mau làm điều ta muốn vào khoảnh khắc này đê
Hãy trao cho anh
Hãy trao cho anh
Hãy trao anh trao cho anh đi những yêu thương nồng cháy
Trao anh ái ân nguyên vẹn đong đầy


Em cho ta ngắm thiên đàng vội vàng qua chốc lát
Như thanh âm chứa bao lời gọi mời trong khúc hát
Liêu xiêu ta xuyến xao rạo rực khát khao trông mong
Dịu dàng lại gần nhau hơn dang tay ôm em vào lòng
Trao đi trao hết đi đừng ngập ngừng che dấu nữa
Quên đi quên hết đi ngại ngùng lại gần thêm chút nữa
Chìm đắm giữa khung trời riêng hai ta như dần hòa quyện mắt nhắm mắt tay đan tay hồn lạc về miền trăng sao

Em cho ta ngắm thiên đàng vội vàng qua chốc lát
Như thanh âm chứa bao lời gọi mời trong khúc hát
Liêu xiêu ta xuyến xao rạo rực khát khao trông mong
Dịu dàng lại gần nhau hơn dang tay ôm em vào lòng
Trao đi trao hết đi đừng ngập ngừng che dấu nữa
Quên đi quên hết đi ngại ngùng lại gần thêm chút nữa
Chìm đắm giữa khung trời riêng hai ta như dần hòa quyện mắt nhắm mắt tay đan tay hồn lạc về miền trăng sao

Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh
Hãy trao cho anh thứ anh đang mong chờ', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 246, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (28, N'quynhln', N'https://ik.imagekit.io/nav26/MUZIC/song/LoiDuongMat-LylyHIEUTHUHAI_QHetZTbYNNl.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Lời đường mật', N'Lyly, HIEUTHUHAI', N'Nhạc trẻ', N'1. Nếu anh nhắn lần đầu là do anh ấn tượng
Và sau đó nếu có lần 2 là do không sai gu
Nếu anh nhắn cho em lần 3 là anh đang vấn vương
Còn nếu mà có nhắn lần 4 so then i like you

3 câu đầu đủ làm em đổ như là haiku
Phải lặp lại câu cuối 2 lần em ơi i like you
Người như em thì chỉ có 1, im loosing my cool
Quay trở về số 0, anh ngại ngùng bên my boo

Lời nói của anh có lời nào là lời thật không
Em liệu có thể tin ai đó chỉ toàn lời đường mật
Mới gặp vài ba hôm chắc mình cần nhiều thời hơn
Để em biết anh, biết anh muốn đến với em bằng cách thức nào

Please thật sự em là con gái nên phải suy nghĩ hơi lâu
Please đừng vội vàng chỉ nói lời ngoài tai em sợ ong bướm sẽ vây lấy anh
Lấy anh lấy anh lấy anh oh wooo

Này lời ngọt ngào chỉ riêng em nghe thôi đấy
Anh đừng đi nói lung tung
Anh đừng đi nói lung tung cả ngày

2. Em làm tim rơi mất một nhịp nên anh đâm đơn kiện
Nói về em toàn lời hoa mỹ là do anh thơm miệng
Đoạn này là anh đang flow nhanh cho nó mau đó
Bởi vì em chính là Nàng Xuân, anh nồi cơm điện

Nếu còn lo là anh đào hoa thì em ơi có bao giờ
Hãy ở bên cạnh anh, baby one more hour
Ghim điện rồi đợi tiếng bíp bíp
Gửi bài hát anh viết viết

Để nấu cho tình yêu hai ta sẽ nở ra cùng
Nếu em muốn coi bói, thì cho anh xem tay
Em sẽ ngã vào lòng của anh ngay đêm nay
Nếu anh là Thịnh Suy em là 1 đêm say

Vì anh như là cân đẩu vân còn em thì trên mây
Mang hình ảnh anh đang tìm kiếm em như là Google
Kiệt tác không phải là doodle
Ứng cử bên Juliet thì anh nặng kí gọi anh Romeo

Với nụ cười đó thì phải lưu giữ trong viện bảo tàng
Cư xử như vị hảo hán, vì lịch sự thuộc diện lão làng
Rồi bước đến bên em, anh không có đi sai đâu
Bảo vệ em khỏi đêm buồn chán, anh thuộc đội G.I Joe

Anh có một ý là uống hết ly này rồi ta lên ban coông
Hai đôi môi quấn chặt cho đến khi mai sau
Dù không phải là Peter Pan, em làm anh yêu hết lớn
Cứ yên tâm là sẽ vừa lòng, vì tim anh đây có size

Em chính là lý do khiến anh đây rap love
Lý do duy nhất làm em sẽ yêu anh tên là HIEUTHUHAI
Có trái tim nhảy khỏi lồng ngực và đi theo em thôi
Sẽ cứ mãi ở đây nếu đặt nụ hôn anh lên môi

Lời nói của anh có lời nào là lời thật không
Em liệu có thể tin ai đó chỉ toàn lời đường mật
Mới gặp vài ba hôm chắc mình cần nhiều thời hơn
Để em biết anh, biết anh muốn đến với em bằng cách thức nào

Please thật sự em là con gái nên phải suy nghĩ hơi lâu
Please đừng vội vàng chỉ nói lời ngoài tai em sợ ong bướm sẽ vây lấy anh
Lấy anh lấy anh lấy anh oh wooo

Này lời ngọt ngào chỉ riêng em nghe thôi đấy
Anh đừng đi nói lung tung
Anh đừng đi nói lung tung cả ngày

Liều thuốc tình yêu này sẽ không làm hại ta đâu em ơi
Có trái tim nhảy khỏi lồng ngực và đi theo em thôi

Lời nói của anh có lời nào là lời thật không
Em liệu có thể tin ai đó chỉ toàn lời đường mật', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 222, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (29, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/MuonRoiMaSaoCon-SonTungMTP_DR1Z7F7C6.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Muộn rồi mà sao còn', N'Sơn Tùng M-TP', N'Nhạc trẻ', N'Muộn rồi mà sao còn nhìn lên trần nhà rồi quay ra lại quay vào
Nằm trằn trọc vậy đến sáng mai
Ôm tương tư, nụ cười của ai đó
Làm con tim ngô nghê như muốn khóc òa
Vắt tay lên trên trán mơ mộng
Được đứng bên em trong nắng xuân hồng
1 giờ sáng trôi qua trôi nhanh kéo theo ưu phiền miên man
Âm thầm gieo tên em vẽ lên hi vọng
Đúng là yêu thật rồi còn không thì hơi phí này cứ thế loanh quanh loanh
Quanh loanh quanh lật qua lật lại (2 giờ)
Những ngôi sao trên cao là người bạn tâm giao
Lắng nghe anh luyên thuyên về một tình đầu đẹp tựa chiêm bao
Có nghe thôi đã thấy ngọt ngào
Đủ biết anh si mê em nhường nào
Ít khi văn thơ anh dạt dào bụng đói nhưng vui quên luôn cồn cào
Nắm đôi tay kiêu sa được một lần không ta
Nghĩ qua thôi con tim trong anh đập tung lên rung nóc rung nhà
Hóa ra yêu đơn phương một người
Hóa ra khi tơ vương một người 3 giờ đêm vẫn ngồi cười

Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong cơn mơ trong cơn mơ trong cơn mơ trong cơn mơ
Có thế cũng khiến anh vui điên lên ngỡ như em đang bên
Chắp bút đôi ba câu thơ ngọt ngào muốn em đặt tên
Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong giấc mơ trong cơn mơ trong cơn mơ trong cơn mơ
Yêu đến vậy thôi phát điên rồi làm sao giờ

[Rap:]
Chịu!
Đêm nay không ngủ
Tay kê lên tủ
Miên man anh tranh thủ chơi với suy tư bao nhiêu cho đủ
Yêu em ngu ngơ mình tôi
Yêu không quan tâm ngày trôi
Yêu ánh mắt bờ môi
Yêu đơn phương vậy thôi
Lại còn chối
Con tim thẹn thùng đập lạc lối liên hồi
Đừng chày cối
Miệng cười cả ngày vậy là chết toi rồi
Ngày càng nhiều thêm
Tình yêu cho em ngày càng nhiều thêm
Muốn nắm đôi bàn tay đó một lần
Du dương chìm sâu trong từng câu ca dịu êm

[Ver 2:]
Những ngôi sao trên cao
Là người bạn tâm giao
Lắng nghe anh luyên thuyên về một tình đầu đẹp tựa chiêm bao
Có nghe thôi đã thấy ngọt ngào
Đủ biết anh si mê em nhường nào
Ít khi văn thơ anh dạt dào bụng đói nhưng vui quên luôn cồn cào
Nắm đôi tay kiêu sa được một lần không ta
Nghĩ qua thôi con tim trong anh đập tung lên rung nóc rung nhà
Hóa ra yêu đơn phương một người
Hóa ra khi tơ vương một người 3 giờ đêm vẫn ngồi cười

Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong cơn mơ trong cơn mơ trong cơn mơ trong cơn mơ
Có thế cũng khiến anh vui điên lên ngỡ như em đang bên
Chắp bút đôi ba câu thơ ngọt ngào muốn em đặt tên
Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong giấc mơ trong cơn mơ trong cơn mơ trong cơn mơ
Yêu đến vậy thôi phát điên rồi làm sao giờ

Em xinh như một thiên thần
Như một thiên thần
Như một thiên thần
Ngỡ như em là thiên thần
Em xinh như một thiên thần
Như một thiên thần

Em xinh như một thiên thần
Như một thiên thần
Như một thiên thần
Ngỡ như em là thiên thần
Ngỡ như ngỡ như ngỡ như ngỡ như ngỡ như

Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong cơn mơ trong cơn mơ trong cơn mơ trong cơn mơ
Có thế cũng khiến anh vui điên lên ngỡ như em đang bên
Chắp bút đôi ba câu thơ ngọt ngào muốn em đặt tên
Cứ ôm anh đi ôm anh đi ôm anh đi ôm anh đi
Ôm trong giấc mơ trong cơn mơ trong cơn mơ trong cơn mơ
Yêu đến vậy thôi phát điên rồi làm sao giờ', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 276, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (30, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/MuonRuouToTinh-EmilyBigDaddy_Pka8Rtan7.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Mượn rượu tỏ tình', N'BigDaddy, Emily', N'Nhạc trẻ', N'BIGDADDY: ohhh, sự chú ý của ta lỡ va phải vào ánh mắt của nàng
Rồi bùng lên trong tim ta như một đốm lửa vàng
Act cool đứng hình mất 5 giây
Nhìn em bên ngoài có lẽ ăn đứt mấy tấm hình đăng face
Hey em ăn tối chưa nhở? Nếu rồi thì làm 1 ly
Anh chỉ muốn cởi mở hơn, đừng lo mình sẽ dại dột đi
Em là cô gái đẹp, phải nói với em bằng những lời hay ho
Được ngồi cùng em khiến anh cảm thấy mình cũng rất gì và này nọ
Em đang thôi miên anh bằng nụ cười thật duyên
Cho anh cảm giác này, em là người đầu tiên
Ngồi đây uống ly whisky này nhưng lại say em là chính
Anh nói thật đấy không *** đùa đâu em đừng nghĩ đó là thính
Từng giờ đồng hồ chậm trôi mà chả 1 ai thèm quan tâm đến
Ly này ly khác, 3 say chưa chai 2 ta mới chỉ hơi chuếnh
Anh mượn rượu để được giãi bày những điều nói ra vốn khó
À thì mà là anh đã kết em rồi đó, nói điêu làm chó

EMILY: Uống đi nào hôm nay trời rất đẹp
Chính ra em cũng hơi buồn lúc anh
Đứng bên ai kia khoác tay ai kìa
Thôi em say rồi đừng chấp em

Vẫn biết rằng mình là bạn bè bao lâu nay
Thế nhưng liệu, liệu rằng lòng anh có đang thấy rung động khi gần em
Say là cơ hội dễ nhất để
Để người ta chân thật

Em mượn rượu tỏ tình đấy thì sao nào
Em yêu anh yêu anh đấy thì sao nào
Hu hú hu hú hu...Hu hú hu hu hu
Hi vọng anh đừng ngơ ngác đừng cau mày
Thôi coi như chưa nghe thấy cạn ly vậy
Hu hú hu hú hu...Hu hú hu hu hu

BIGDADDY: Và dường như là cô ta đã thích tôi
Ta vẫn uống hết không 1 ly nào nhấp môi
Cả 2 say sưa và cùng gần lại sát ngồi
Kề vai bên tôi em nói rằng chỉ thích nghe Bích rap thôi
Ta sẽ lêu hêu khắp phố phường vì giờ này còn sớm chán
Những nụ hôn, cái ôm mặc kệ cho trời tới sáng
Em có uống được tiếp hay không thì mình có thể thả phanh
Còn nếu như em đồng ý thì sau đó có thể về nhà anh

EMILY: Ta đi chơi tiếp đi bên nhau cho hết đêm dài
Bao lâu không biết say không được thoải mái như vậy..
Sợ rằng vào ngày mai 2 ta lại ngại ngùng nhau đôi khi không *** nhìn mặt
Mặc kệ đời đi quan tâm chi chỉ cần ngày hôm nay 2 ta thích nhau thật...

Em mượn rượu tỏ tình đấy thì sao nào
Em yêu anh yêu anh đấy thì sao nào
Hu hú hu hú hu...Hu hú hu hu hu
Em tỏ ra hồn nhiên đấy thì sao nào..
Em yêu anh yêu anh đấy thì sao nào
Hu hú hu hú hu...Hu hú hu hu hu', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 198, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (31, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/PhaiChangEmDaYeu-JukySanRedT_ugRMNApvR.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Phải chăng em đã yêu', N'Juky San, RedT', N'Nhạc trẻ', N'Phải chăng em đã yêu ngay từ cái nhìn đầu tiên
Phải chăng em đã say ngay từ lúc thấy nụ cười ấy
Tình yêu ta ngất ngây, xây được chín tầng trời mây, khuất xa mờ
Ánh lên từng giấc mơ ngày có anh.

Cuộc đời em vốn, chỉ là đường thẳng mà thôi
Mà sao tình cờ gặp anh, em rẽ ngang qua đời
Vài người vội vã, vội đến rồi đi
Mà sao em yêu anh đâu cần nghĩ suy

Từng đêm nhớ mong về người
Biết anh còn chờ đợi
Chơi vơi bao đêm em thấy đủ rồi

Ngàn tia nắng anh gần lại
Ánh dương màu mắt xanh ngời
Lóe lên ngàn giấc mơ còn trong đời

[ĐK:]
Phải chăng em đã yêu ngay từ cái nhìn đầu tiên
Phải chăng em đã say ngay từ lúc thấy nụ cười ấy
Tình yêu ta ngất ngây, xây được chín tầng trời mây, khuất xa mờ
Ánh lên từng giấc mơ ngày có anh.

Trái đất vốn lạ thường
Mà sao em cứ đi nhầm đường
Lạc vào tim anh lẻ loi
Đằng sau chữ yêu đây là thương

When you call me a baby
Make me so crazy
My heart is breaking slowly
Chầm chậm bờ môi khẽ trôi
Ôi mình yêu thật rồi

Tình cờ biết nhớ những lúc ngây thơ
Tình cờ lắm lúc chỉ biết vu vơ
Tình cờ bơ vơ lạc trong những giấc mơ

Lắm lúc chỉ muốn nói anh mãi thương em
Nhưng trong tim bâng khuân chẳng biết có ai xem này
Vì chữ thương nặng lắm đâu thể phơi bày

[ĐK:]
Phải chăng em đã yêu ngay từ cái nhìn đầu tiên
Phải chăng em đã say ngay từ lúc thấy nụ cười ấy
Tình yêu ta ngất ngây, xây được chín tầng trời mây, khuất xa mờ
Ánh lên từng giấc mơ ngày có anh.

Yêu hay không yêu
Thương em anh hãy nói
Trao nhau đôi môi
Rồi sẽ trở thành đôi

Em đang chơi vơi
Liệu anh có bước tới
Chầm chậm nói đôi lời, khiến em chợt vui cười', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 190, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (32, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/SaiGonDauLongQua-HuaKimTuyenHoangDuyen_cW7yi7fVy.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg', N'Sài Gòn đau lòng quá', N'Hứa Kim Tuyền, Hoàng Duyên', N'Nhạc trẻ', N'Cầm tấm vé trên tay
Em bay đến nơi xa
Sài Gòn đau lòng quá
Toàn kỷ niệm chúng ta.
Phải đi xa đến đâu?
Thời gian quên mất bao lâu?
Để trái tim em bình yên như ngày đầu tiên...

Mình đã từng hứa
Bên nhau hết tháng năm dài
Yêu đến khi ngừng hơi thở
Đến khi ngừng mơ...
Nắm chặt tay đi hết nhân thế này
Chân trời hằn chân ta
Vô tận là chúng ta...

Mình đã từng hứa
Đi qua hết bao thăng trầm
Cho dẫu mai này xa rời
Vẫn không hề đổi dời...
Có ngờ đâu, đã sớm vỡ tan tành
Nhặt từng mảnh vỡ xếp vào vali...

Cứ càng yêu, cứ càng đau
Cứ càng quên rồi lại muốn đi thật nhiều
Tokyo hay Seoul
Paris hay New York
Đi càng xa, càng không thể quên...

Cầm tấm vé trên tay
Em bay đến nơi xa
Sài Gòn đau lòng quá
Toàn kỷ niệm chúng ta.
Phải đi xa đến đâu?
Thời gian quên mất bao lâu?
Để trái tim em bình yên như ngày đầu tiên...

Ngày tôi chưa từng biết tôi sẽ yêu em nhiều như thế này
Để rồi khi ta cách xa tim này nát ra
Ngày người chưa đến mang theo giấc mơ, rồi lại bỏ rơi lúc tôi đang chờ...
Chờ người đến dịu xoa tổn thương tôi đã từng...', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 309, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (33, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/SangMatChua-TrucNhan_YutUTF79sHK.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg  ', N'Sáng mắt chưa', N'Trúc Nhân', N'Nhạc trẻ', N'Ba mẹ anh từng nói
Yêu ai phải yêu xứng đôi vừa lứa
Hình như không ai dạy em
Nên em còn non
Em chưa học qua
Cách chọn đúng người
Chàng trai đi cùng đang ăn cây kem
Thật lòng không xứng với em
Em nhìn anh rồi xem
Ai đi với ai xứng đôi vừa lứa?
Người xinh như em
Mà ngây ngô ghê
Rồi sau người ta lại nói
Phí đời!
“Phí quá chời luôn á”
Hình như em cần vitamin A
Để cho mình sáng mắt ra
Đừng theo người đó
Em ơi em à
Đừng gây thù oán
Đau lòng mẹ cha
Chẳng hiểu em cứ níu kéo vô vọng làm chi
Vì sớm hay muộn rồi thì anh ấy cũng đi
Thời gian thấm thoát thoi đưa
Thể nào anh cũng sẽ lừa được em
Chàng trai đang sánh bước bên em
Đằng nào rồi cũng... sẽ thuộc về anh
Oh oh oh...
Chàng trai đang sánh bước bên em
Đằng nào rồi cũng sẽ thuộc về anh
Em nhìn em rồi xem
Em sai một li em đi một thước
Giờ anh muốn bên cạnh em
Muốn ôm chặt em
Rồi nhẹ nhàng nói
“Đáng đời”
Anh đã nói rồi
Người ta đang chờ anh trong cơn mưa
Giờ em đã sáng mắt chưa.
Đừng theo người đó em ơi em à
Đừng gây thù oán
Đau lòng mẹ cha
Chẳng hiểu em cứ níu kéo vô vọng làm chi
Vì sớm hay muộn rồi thì anh ấy cũng đi
Thời gian thấm thoát thoi đưa
Thể nào anh cũng sẽ lừa được em
Chàng trai đang sánh bước bên em
Đằng nào rồi cũng... sẽ thuộc về anh
Oh oh oh...
Chàng trai đang sánh bước bên em
Đằng nào rồi cũng sẽ thuộc về anh

Sảmi Khỏong Khun Pên Key
Your Husband Is Gay

Anh ta trông thật yêu yêu
Còn nhìn em trông thật eo ôi!!
Tình yêu là một giấc mộng đẹp
Nhưng em cuối cùng lại khóc ướt nhẹp cái gối nè
Lần sau em kiếm bà mối nào tốt hơn nghen
Anh không phải là người hay ghen
Anh chỉ nói để em biết là chân em còn phèn
Anh không giỏi anh trông bình bình
Anh chỉ giỏi lấy lại những thứ là của mình
Em xinh xắn em trông đứng đắn nên thu xếp
Khi thái độ anh còn ngay ngắn nha!!', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 208, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (34, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/TuChoiNheNhangThoi-BichPhuongPhucDu_cKPZtVQls.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg  ', N'Từ chối nhẹ nhàng thôi', N'Bích Phương, Phúc Du', N'Nhạc trẻ', N'[Bích Phương]
Tình cảm này khó nói
Nếu thật sự chẳng thể giấu nữa
Thôi đừng lo em hứa
Rằng sẽ từ chối anh nhẹ nhàng

Tình cảm này khó nói
Nếu thật sự chẳng thể né tránh
Nói thật nhanh đi anh
Để em từ chối anh nhẹ nhàng thôi

[Phúc Du]
Ta thường mơ về một thoáng chốc được bên nàng
Tâm hồn say lạc giữa thành phố một đêm vàng
Dưới ánh đèn đường hiu hắt, tương tư văng vẳng không im
Nàng chỉ gửi một ánh mắt, ta đã trao cả con tim

Giữa biển người nhưng anh chỉ có em trong mắt
Giữa một rừng thanh âm chỉ nghe thấy tiếng em trong vắt
Đêm xuống, tâm tư anh là nhà giam
Với nỗi nhớ về em là song sắt

Nhưng tình cảm đâu phải nhành cây, nên nó đâu có dễ
Ta như vai áo em buông, bởi vì ta quá trễ
Giờ đến và cướp em đi, là điều ta không được làm
Vì sợi tơ hồng đang buộc ta, nó lại không buộc nàng

Nên nếu em không thể đưa ra lựa chọn
Thì xin em hãy sớm đẩy anh đi xa
Nếu bờ môi ấy không thể cho anh vị ngọt
Thì em hãy để nó buông cho anh lời vị tha

[Bích Phương]
Ở bên anh em thấy trái tim thổn thức cứ không thể dứt, babe
Nhưng ta không nên xem chương tiếp theo
Vì chuyện tình này có kết cục buồn
Mà em không mong muốn thấy anh buồn bã thế đâu dừng bước đi anh
Đôi khi em bâng khuâng khi nghĩ rằng
Sự thật là mình không thể bên nhau

Tình cảm thật khó nói
Em nhìn anh mà lòng bối rối
Muộn màng rồi anh ơi
Bởi vì người đến sau mất rồi

Vào một ngày nắng tắt
Lỡ nhận trao nhầm một ánh mắt
Cô là người đáng trách
Kẻ tình si đáng quên lại là anh

[Phúc Du]
Anh vẫn cần em nhiều lắm
Như một rừng cây ở giữa trời đông đang cần một chiều nắng
Nàng lạnh lùng như tảng băng trôi, khi ở xa mà ta nhìn ngắm
Anh đâu phải Titanic sao cứ gặp em là anh chìm đắm

Ta đã cố để tìm cách khác nhưng nó không nằm trong sách vở
Cách duy nhất mà ta có dường như chỉ là cách trở
Em như là một cơn nghiện vẫn dày vò anh nhiều quá
Nàng đã cho ta có cửa nhưng sao lại tiếc cái chìa khóa?

Vì thương nàng nên giấc không yên, ta vẫn chẳng thể nào ngủ
Lí trí là đứa nhân viên vì nó thật khó để làm chủ
Đếm từng ngày xa lòng ta nặng nề như đá đổ
Anh mang tình cảm lớn người ta bảo là quá khổ

Không có nơi nào có thể là vườn địa đàng
Nếu như nơi đó em không thể ở gần anh
Anh chẳng muốn nghe một lời từ chối nhẹ nhàng
Hãy cho hi vọng của anh một cái chết thật nhanh

[Bích Phương]
Tình cảm thật khó nói
Em nhìn anh mà lòng bối rối
Muộn màng rồi anh ơi
Bởi vì người đến sau mất rồi

Vào một ngày nắng tắt
Lỡ nhận trao nhầm một ánh mắt
Cô là người đáng trách
Kẻ tình si đánh quên lại là anh

Người có thể hờn trách em khi mà em luôn thức đêm bỏ mặc anh trong giấc mơ ngày hay đêm vẫn mơ
Hoặc có thể quên lãng em khi mà em không dắt tay anh vào trong nơi trái tim bỏ mặc anh đứng im

Tình cảm này khó nói
Nếu thật sự chẳng thể giấu nữa
Thôi đừng lo em hứa
Rằng sẽ từ chối anh nhẹ nhàng

Tình cảm này khó nói
Nếu thật sự chẳng thể né tránh
Nói thật nhanh đi anh
Để em từ chối anh nhẹ nhàng thôi', N'Bài này chill phết!', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 247, 1, 0)
INSERT [dbo].[Song] ([Id], [Username], [Src], [Thumbnail], [Cover], [Title], [Artist], [Genre], [Lyrics], [Description], [Date], [Duration], [IsPublic], [Status]) VALUES (35, N'vuna', N'https://ik.imagekit.io/nav26/MUZIC/song/VetMua-VuCatTuong_-l7MNBj1f.mp3', N'/img/upload-thumbnail.jpg', N'/img/upload-cover.jpg ', N'Vết mưa', N'Vũ Cát Tường', N'Nhạc trẻ', N'Cơn mưa, đã xoá hết những ngày yêu qua
Chỉ còn mình anh ngu ngơ, mong cho cơn
Tan trong yêu thương không vội vã

Mưa ngoan, giấu hết những phút thẫn thờ này
Thương em đi giữa đêm lạnh
Khoảng trời một mình
Bỏ lại tình mình theo làn mây

Tìm về ngày yêu ấy
Cũng trong chiều mưa này
Mình đã gặp nhau, lạnh bờ vai
Nhưng tim vẫn cười

Giờ vẫn chiều mưa ấy
Em nép trong vòng tay ai?
Anh chỉ lặng im,
Đôi hàng mi nhẹ rung cho tim anh bật khóc

Chorus:

Đã qua rồi, qua khoảnh khắc đôi mình
Nói tiếng yêu ngập ngừng,
Rồi nhẹ nhàng đặt lên môi hôn
Cho anh quên đi lạnh giá

Vỡ tan rồi, anh chẳng nói nên lời
Mưa rơi xé tan bóng hình
Vì giờ này em quay đi
Buông tay anh trong chiều giá lạnh

[ĐK:]
Khi, cơn mưa cuốn hết nỗi đau ấy
Anh sẽ quên, những yêu thương
Anh viết riêng cho em
Khi, cầu vồng lên sau cơn bão giông
Anh sẽ đi qua yêu thương,
Không còn vấn vương...', N'Bài này chill phết', CAST(N'2022-03-21T15:37:10.213' AS DateTime), 210, 1, 0)
SET IDENTITY_INSERT [dbo].[Song] OFF
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 2, CAST(N'2022-03-21T20:03:51.733' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 2, CAST(N'2022-03-21T20:03:52.377' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 2, CAST(N'2022-03-21T20:03:52.877' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 2, CAST(N'2022-03-21T20:03:53.397' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 2, CAST(N'2022-03-21T20:03:54.017' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 7, 2, CAST(N'2022-03-21T20:03:56.227' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 29, 2, CAST(N'2022-03-21T20:03:56.700' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 2, CAST(N'2022-03-21T20:03:57.767' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 32, 2, CAST(N'2022-03-21T20:03:58.610' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 34, 2, CAST(N'2022-03-21T20:03:59.743' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 35, 2, CAST(N'2022-03-21T20:04:00.197' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 22, 2, CAST(N'2022-03-21T20:04:06.127' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 2, CAST(N'2022-03-21T20:04:06.513' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 24, 2, CAST(N'2022-03-21T20:04:07.207' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 25, 2, CAST(N'2022-03-21T20:04:08.323' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 26, 2, CAST(N'2022-03-21T20:04:08.740' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 28, 2, CAST(N'2022-03-21T20:04:09.963' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 15, 2, CAST(N'2022-03-21T20:04:15.360' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 16, 2, CAST(N'2022-03-21T20:04:15.833' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 8, 2, CAST(N'2022-03-21T20:04:20.603' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 9, 2, CAST(N'2022-03-21T20:04:21.050' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 10, 2, CAST(N'2022-03-21T20:04:21.617' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 11, 2, CAST(N'2022-03-21T20:04:22.687' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 12, 2, CAST(N'2022-03-21T20:04:23.240' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 8, 2, CAST(N'2022-03-21T20:04:48.257' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 9, 2, CAST(N'2022-03-21T20:04:48.653' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 10, 2, CAST(N'2022-03-21T20:04:48.983' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 10, 3, CAST(N'2022-03-21T20:04:49.310' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 11, 2, CAST(N'2022-03-21T20:04:49.770' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 10, 2, CAST(N'2022-03-21T20:04:50.153' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 12, 2, CAST(N'2022-03-21T20:04:50.903' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 13, 2, CAST(N'2022-03-21T20:04:51.913' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 14, 2, CAST(N'2022-03-21T20:04:52.363' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 15, 2, CAST(N'2022-03-21T20:04:53.263' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 16, 2, CAST(N'2022-03-21T20:04:53.617' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 19, 2, CAST(N'2022-03-21T20:04:54.730' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 24, 2, CAST(N'2022-03-21T20:04:59.323' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 25, 2, CAST(N'2022-03-21T20:04:59.667' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 26, 2, CAST(N'2022-03-21T20:05:00.170' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 28, 2, CAST(N'2022-03-21T20:05:01.677' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 2, CAST(N'2022-03-21T20:05:24.837' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 2, CAST(N'2022-03-21T20:05:25.177' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 2, CAST(N'2022-03-21T20:05:25.540' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 2, CAST(N'2022-03-21T20:05:26.137' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 2, CAST(N'2022-03-21T20:05:26.977' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 6, 2, CAST(N'2022-03-21T20:05:27.357' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 7, 2, CAST(N'2022-03-21T20:05:28.507' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 8, 2, CAST(N'2022-03-21T20:05:29.890' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 9, 2, CAST(N'2022-03-21T20:05:30.223' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 10, 2, CAST(N'2022-03-21T20:05:32.790' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 11, 2, CAST(N'2022-03-21T20:05:33.123' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 28, 2, CAST(N'2022-03-21T20:05:38.510' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 27, 2, CAST(N'2022-03-21T20:05:39.987' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 29, 2, CAST(N'2022-03-21T20:05:41.167' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 2, CAST(N'2022-03-21T20:05:58.160' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 2, CAST(N'2022-03-21T20:05:58.497' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 2, CAST(N'2022-03-21T20:05:58.870' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 4, 2, CAST(N'2022-03-21T20:05:59.250' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 5, 2, CAST(N'2022-03-21T20:05:59.713' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 6, 2, CAST(N'2022-03-21T20:06:00.270' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 7, 2, CAST(N'2022-03-21T20:06:00.597' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 7, 3, CAST(N'2022-03-21T20:06:00.883' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 2, CAST(N'2022-03-21T20:06:15.543' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 2, CAST(N'2022-03-21T20:06:15.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 2, CAST(N'2022-03-21T20:06:16.310' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 3, CAST(N'2022-03-21T20:06:18.180' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 2, CAST(N'2022-03-21T20:06:18.600' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 2, CAST(N'2022-03-21T20:06:18.910' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 2, CAST(N'2022-03-21T20:06:19.457' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 2, CAST(N'2022-03-21T20:06:19.993' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 2, CAST(N'2022-03-21T20:06:20.450' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 10, 2, CAST(N'2022-03-21T20:06:21.667' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 10, 3, CAST(N'2022-03-21T20:06:21.990' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 11, 2, CAST(N'2022-03-21T20:06:22.540' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 10, 2, CAST(N'2022-03-21T20:06:22.893' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 12, 2, CAST(N'2022-03-21T20:06:23.547' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 2, CAST(N'2022-03-21T20:06:29.310' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:34.987' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:37.073' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:38.163' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:40.623' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:41.107' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:41.340' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:41.507' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:41.787' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:41.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:42.193' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 1, 1, CAST(N'2022-03-21T20:06:42.903' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:46.197' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:46.787' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:47.203' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:47.337' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:47.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:47.817' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:47.977' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:48.220' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:48.340' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:48.587' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 2, 1, CAST(N'2022-03-21T20:06:48.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:50.903' AS DateTime))
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:52.043' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:52.237' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:52.393' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:52.627' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:52.773' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:53.137' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 3, 1, CAST(N'2022-03-21T20:06:53.317' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:54.997' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:56.030' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:56.370' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:56.620' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:56.810' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:56.990' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:57.380' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:57.600' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:57.830' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:58.193' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:58.420' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 4, 1, CAST(N'2022-03-21T20:06:58.550' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:00.143' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:00.933' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:01.210' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:01.427' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:01.563' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:01.800' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:01.983' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:02.170' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:02.380' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:02.503' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:02.747' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:02.910' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:03.073' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:03.303' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:03.470' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:03.657' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 5, 1, CAST(N'2022-03-21T20:07:03.777' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:05.957' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:06.677' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:07.067' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:07.250' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:07.613' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:07.907' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:08.063' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:08.310' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:08.450' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:08.897' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:09.067' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:09.227' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:09.463' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 6, 1, CAST(N'2022-03-21T20:07:09.603' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:11.237' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:12.047' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:12.450' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:12.860' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:13.080' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:13.500' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:13.667' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:13.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:14.083' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:14.533' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:14.723' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:14.903' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:15.077' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 7, 1, CAST(N'2022-03-21T20:07:15.377' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:17.100' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:18.063' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:18.283' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:18.667' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:18.963' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:19.267' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:19.507' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:19.923' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:20.320' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'vuna', 8, 1, CAST(N'2022-03-21T20:07:20.500' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:30.827' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:31.640' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:32.010' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:32.420' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:32.810' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:33.403' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:33.593' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:33.760' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:33.997' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:34.143' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:34.410' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:34.550' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:34.803' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-21T20:07:34.977' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:36.180' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:36.520' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:36.763' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:36.910' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:37.290' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:37.527' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:37.647' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:38.057' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:38.413' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:38.643' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:38.817' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:39.173' AS DateTime))
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-21T20:07:39.537' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:40.767' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:41.497' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:41.737' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:41.860' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:42.253' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:42.473' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:42.607' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:42.880' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:43.263' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:43.493' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:43.650' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:43.873' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:44.073' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:44.250' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:44.417' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 3, 1, CAST(N'2022-03-21T20:07:44.577' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:45.853' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:47.357' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:47.617' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:47.870' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:48.017' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:48.273' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:48.447' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:49.223' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 4, 1, CAST(N'2022-03-21T20:07:49.587' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:51.303' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:51.643' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:51.877' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:52.020' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:52.240' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:52.460' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:52.620' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:52.850' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 5, 1, CAST(N'2022-03-21T20:07:53.463' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:19.677' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:20.363' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:20.690' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:20.943' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:21.143' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:21.483' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:21.690' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:21.870' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:22.030' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:22.270' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:22.427' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:22.603' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:22.833' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:22.967' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:23.220' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:23.377' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 1, 1, CAST(N'2022-03-21T20:08:23.670' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:25.347' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:26.103' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:26.330' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:26.707' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:26.950' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:27.103' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:27.547' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:27.810' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:27.963' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:28.203' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:28.350' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:28.613' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:28.800' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:29.250' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:29.443' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:29.597' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 2, 1, CAST(N'2022-03-21T20:08:29.850' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:31.193' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:31.777' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:32.040' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:32.413' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:32.660' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:32.820' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:33.250' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:33.493' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:33.690' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:33.950' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:34.150' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:34.317' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 3, 1, CAST(N'2022-03-21T20:08:34.713' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:36.113' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:37.343' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:37.563' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:37.820' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:38.053' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:38.217' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:38.460' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:38.643' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:38.923' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:39.077' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:39.457' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:39.753' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:39.897' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:40.207' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:40.367' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 4, 1, CAST(N'2022-03-21T20:08:40.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:41.953' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:42.933' AS DateTime))
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:43.187' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:43.430' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:43.580' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:43.810' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:44.027' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:44.263' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:44.460' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:44.637' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:45.043' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duydnt', 5, 1, CAST(N'2022-03-21T20:08:45.197' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:09.807' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:10.303' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:10.660' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:10.903' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:11.050' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:11.283' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:11.520' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:11.673' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:11.923' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:12.140' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:12.313' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:12.553' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:12.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:13.350' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:13.713' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:14.137' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:14.407' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:14.537' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:14.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:15.103' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:15.323' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 1, 1, CAST(N'2022-03-21T20:09:15.460' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:16.690' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:17.550' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:17.763' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:18.003' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:18.160' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:18.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:18.837' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:18.960' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:19.240' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:19.397' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:19.650' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:19.807' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:20.080' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:20.310' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:20.493' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:20.733' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:20.893' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:21.307' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:21.730' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 2, 1, CAST(N'2022-03-21T20:09:21.920' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:23.077' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:23.877' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:24.123' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:24.323' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:24.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:24.727' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:25.167' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:25.610' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:26.087' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:26.287' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:26.490' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:26.853' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 3, 1, CAST(N'2022-03-21T20:09:26.990' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:28.370' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:29.547' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:29.813' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:30.057' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:30.217' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:30.573' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:30.837' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:31.017' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:31.403' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:31.667' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:31.807' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:32.020' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 4, 1, CAST(N'2022-03-21T20:09:32.180' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:34.170' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:35.010' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:35.273' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:35.693' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:35.963' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:36.090' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:36.570' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:36.803' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:37.160' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:37.550' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'duynk', 5, 1, CAST(N'2022-03-21T20:09:37.743' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:09:59.193' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:09:59.660' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:00.037' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:00.250' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:00.390' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:00.627' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:00.743' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:01.143' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:01.373' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:01.587' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:01.740' AS DateTime))
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 1, 1, CAST(N'2022-03-21T20:10:01.883' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:03.017' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:03.610' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:03.840' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:03.990' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:04.227' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:04.390' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:04.613' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:04.750' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:05.010' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:05.130' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:05.527' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:05.800' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:05.923' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 2, 1, CAST(N'2022-03-21T20:10:06.070' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:07.273' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:07.697' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:08.063' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:08.320' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:08.477' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:08.940' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:09.153' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:09.347' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:09.810' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:09.937' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:10.170' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:10.323' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:10.870' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 3, 1, CAST(N'2022-03-21T20:10:11.110' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:02.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:04.113' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:04.273' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:04.440' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:04.813' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:05.090' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:05.227' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 1, 1, CAST(N'2022-03-22T18:11:05.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-22T18:11:07.050' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-22T18:11:08.113' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-22T18:11:08.390' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-22T18:11:08.547' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-22T18:11:08.900' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 2, 1, CAST(N'2022-03-22T18:11:09.033' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:11.800' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:12.717' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:12.987' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:13.100' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:13.673' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:13.913' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:14.047' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:14.333' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:14.480' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:14.827' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:15.077' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:15.297' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:15.500' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:15.670' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:15.853' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:16.010' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:16.243' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:18.130' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:18.497' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 23, 1, CAST(N'2022-03-22T18:11:18.830' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:25.560' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:26.200' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:26.490' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:26.610' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:26.973' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:27.237' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:27.360' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:27.767' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:27.990' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:28.210' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:28.410' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:28.740' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:29.030' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:29.187' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:29.330' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'canhdm', 31, 1, CAST(N'2022-03-22T18:11:29.823' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 23, 1, CAST(N'2022-03-22T18:11:54.867' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 23, 1, CAST(N'2022-03-22T18:11:55.887' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 23, 1, CAST(N'2022-03-22T18:11:56.130' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 23, 1, CAST(N'2022-03-22T18:11:56.227' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 23, 1, CAST(N'2022-03-22T18:11:56.623' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 23, 1, CAST(N'2022-03-22T18:11:56.843' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:58.277' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:58.637' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:58.900' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:59.057' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:59.277' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:59.403' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:11:59.810' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:00.197' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:00.480' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:00.647' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:01.040' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:01.340' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:01.577' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:01.873' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:02.007' AS DateTime))
GO
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:02.390' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 1, CAST(N'2022-03-22T18:12:02.763' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 27, 2, CAST(N'2022-03-22T18:12:04.300' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:07.760' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:08.503' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:08.727' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:08.973' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:09.153' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:09.310' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:09.517' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:09.680' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:09.900' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:10.127' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:10.280' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:10.510' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:10.643' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:10.900' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:11.027' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:11.380' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:11.643' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:11.780' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:12.043' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:12.183' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 2, CAST(N'2022-03-22T18:12:12.570' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:12.857' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:13.247' AS DateTime))
INSERT [dbo].[SongTracking] ([Username], [SongId], [EventType], [Date]) VALUES (N'quynhln', 34, 1, CAST(N'2022-03-22T18:12:13.613' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-21T20:03:39.567' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/4', CAST(N'2022-03-21T20:04:03.720' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:04:13.890' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:04:17.940' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:04:26.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:04:26.853' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:04:28.490' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/', CAST(N'2022-03-21T20:04:41.340' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:04:43.783' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:04:46.653' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:04:57.187' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:05:03.513' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:05:03.730' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:05:06.347' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/', CAST(N'2022-03-21T20:05:18.010' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:05:22.810' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/4', CAST(N'2022-03-21T20:05:35.670' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:05:42.453' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:05:42.727' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:05:44.013' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/', CAST(N'2022-03-21T20:05:52.507' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:05:55.220' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:06:02.587' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:06:02.870' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:06:04.060' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/', CAST(N'2022-03-21T20:06:09.187' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:06:12.107' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:06:17.217' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:06:27.780' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:34.953' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:37.053' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:38.153' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:40.610' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:41.090' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:41.320' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:41.490' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:41.770' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:41.920' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:42.183' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:06:42.890' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:46.180' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:46.770' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:47.193' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:47.323' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:47.543' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:47.803' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:47.960' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:48.200' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:48.330' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:48.570' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:06:48.933' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:50.893' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:52.023' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:52.223' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:52.383' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:52.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:52.763' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:53.117' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:06:53.303' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:54.977' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:56.020' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:56.363' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:56.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:56.793' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:56.983' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:57.367' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:57.590' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:57.817' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:58.177' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:58.397' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:06:58.537' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:00.127' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:00.920' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:01.197' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:01.413' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:01.540' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:01.783' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:01.970' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:02.160' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:02.357' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:02.493' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:02.730' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:02.897' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:03.063' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:03.290' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:03.453' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:03.643' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:03.767' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:05.940' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:06.660' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:07.047' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:07.237' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:07.600' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:07.893' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:08.050' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:08.290' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:08.437' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:08.880' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:09.050' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:09.217' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:09.443' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/6', CAST(N'2022-03-21T20:07:09.593' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:11.230' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:12.033' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:12.433' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:12.850' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:13.067' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:13.490' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:13.647' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:13.927' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:14.063' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:14.510' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:14.697' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:14.880' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:15.060' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/7', CAST(N'2022-03-21T20:07:15.363' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:17.090' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:18.050' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:18.270' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:18.647' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:18.923' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:19.250' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:19.493' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:19.900' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:20.303' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/song/8', CAST(N'2022-03-21T20:07:20.483' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'vuna', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:07:21.763' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:07:21.930' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:07:23.763' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/', CAST(N'2022-03-21T20:07:27.613' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:30.810' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:31.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:31.993' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:32.397' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:32.797' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:33.383' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:33.570' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:33.747' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:33.977' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:34.127' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:34.390' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:34.533' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:34.787' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:07:34.967' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:36.167' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:36.510' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:36.747' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:36.887' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:37.273' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:37.510' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:37.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:38.040' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:38.407' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:38.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:38.803' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:39.160' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:07:39.523' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:40.753' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:41.480' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:41.720' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:41.843' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:42.240' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:42.453' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:42.593' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:42.863' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:43.243' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:43.473' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:43.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:43.863' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:44.060' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:44.237' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:44.403' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:07:44.563' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:45.840' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:47.337' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:47.597' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:47.850' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:48.000' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:48.257' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:48.433' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:49.203' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:07:49.570' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:51.287' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:51.633' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:51.853' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:52.007' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:52.223' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:52.440' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:52.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:52.830' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:07:53.453' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:56.457' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:57.160' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:57.413' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:57.790' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:58.180' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:58.590' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:58.740' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:07:59.100' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:00.550' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:01.617' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:01.887' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:02.160' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:02.317' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:02.553' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:02.697' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:03.110' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:03.517' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:03.933' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:04.410' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:05.420' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:05.813' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:06.217' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:06.413' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:06.600' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:07.020' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:07.407' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:07.603' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:07.990' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:08.420' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/3', CAST(N'2022-03-21T20:08:08.580' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:08:09.440' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:08:09.610' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:08:11.120' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/', CAST(N'2022-03-21T20:08:16.340' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:19.663' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:20.347' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:20.673' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:20.933' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:21.100' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:21.470' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:21.670' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:21.850' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:22.013' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:22.257' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:22.410' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:22.590' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:22.810' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:22.953' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:23.203' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:23.367' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:08:23.653' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:25.333' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:26.087' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:26.313' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:26.693' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:26.940' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:27.083' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:27.527' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:27.787' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:27.950' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:28.193' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:28.337' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:28.597' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:28.790' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:29.230' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:29.423' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:29.573' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:08:29.837' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:31.180' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:31.763' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:32.003' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:32.393' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:32.633' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:32.803' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:33.237' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:33.480' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:33.670' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:33.933' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:34.127' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:34.300' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:08:34.697' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:36.090' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:37.330' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:37.547' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:37.807' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:38.033' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:38.200' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:38.443' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:38.627' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:38.910' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:39.060' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:39.437' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:39.737' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:39.880' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:40.170' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:40.347' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:08:40.543' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:41.930' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:42.923' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:43.167' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:43.420' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:43.563' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:43.793' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:44.010' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:44.240' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:44.440' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:44.620' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:45.023' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:08:45.180' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:49.280' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:49.697' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:50.020' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:50.430' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:50.687' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:50.857' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:51.103' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:51.243' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:51.650' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:52.523' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:08:52.900' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:54.247' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:54.947' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:55.913' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:56.267' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:56.633' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:57.057' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:57.350' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:57.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/playlist/2', CAST(N'2022-03-21T20:08:57.830' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duydnt', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:08:58.823' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:08:59.007' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:08:59.923' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/', CAST(N'2022-03-21T20:09:04.833' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:09.793' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:10.290' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:10.643' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:10.883' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:11.037' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:11.270' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:11.500' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:11.660' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:11.907' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:12.120' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:12.300' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:12.540' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:12.923' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:13.337' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:13.697' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:14.113' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:14.387' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:14.520' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:14.917' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:15.087' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:15.300' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:15.447' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:16.673' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:17.537' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:17.750' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:17.983' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:18.143' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:18.540' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:18.820' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:18.943' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:19.217' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:19.380' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:19.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:19.790' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:20.070' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:20.287' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:20.480' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:20.717' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:20.883' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:21.287' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:21.710' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:09:21.903' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:23.060' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:23.860' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:24.110' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:24.307' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:24.540' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:24.707' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:25.153' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:25.590' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:26.057' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:26.267' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:26.460' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:26.833' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:09:26.983' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:28.353' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:29.537' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:29.800' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:30.043' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:30.203' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:30.560' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:30.820' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:31.010' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:31.387' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:31.650' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:31.783' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:32.000' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/4', CAST(N'2022-03-21T20:09:32.170' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:34.153' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:34.997' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:35.260' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:35.673' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:35.943' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:36.077' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:36.553' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:36.790' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:37.147' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:37.530' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/song/5', CAST(N'2022-03-21T20:09:37.730' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:40.980' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:41.433' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:41.767' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:45.463' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:46.040' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:46.280' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:46.690' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:47.433' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:09:47.627' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'duynk', N'http://localhost:5026/logout', CAST(N'2022-03-21T20:09:48.723' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-21T20:09:48.893' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-21T20:09:49.967' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/', CAST(N'2022-03-21T20:09:54.610' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:59.180' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:09:59.643' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:00.007' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:00.230' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:00.380' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:00.610' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:00.730' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:01.130' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:01.360' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:01.537' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:01.720' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/1', CAST(N'2022-03-21T20:10:01.870' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:03.000' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:03.590' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:03.827' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:03.973' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:04.210' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:04.373' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:04.600' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:04.737' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:04.987' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:05.120' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:05.510' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:05.767' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:05.910' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/2', CAST(N'2022-03-21T20:10:06.047' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:07.260' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:07.683' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:08.057' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:08.307' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:08.460' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:08.927' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:09.107' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:09.330' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:09.793' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:09.923' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:10.150' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:10.307' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:10.850' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/3', CAST(N'2022-03-21T20:10:11.100' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:13.450' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:13.907' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:14.180' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:14.390' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:14.523' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:14.910' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:15.197' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:15.347' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:15.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:15.760' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:16.010' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:16.173' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:16.433' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:16.610' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:16.867' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:17.040' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:17.267' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:17.500' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:17.717' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:17.890' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/1', CAST(N'2022-03-21T20:10:18.047' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-22T18:10:11.350' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-22T18:10:54.673' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/', CAST(N'2022-03-22T18:10:58.947' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:02.523' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:04.090' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:04.257' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:04.427' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:04.800' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:05.053' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:05.213' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/1', CAST(N'2022-03-22T18:11:05.543' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-22T18:11:07.033' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-22T18:11:08.103' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-22T18:11:08.373' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-22T18:11:08.533' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-22T18:11:08.887' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/2', CAST(N'2022-03-22T18:11:09.017' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:11.780' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:12.700' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:12.970' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:13.083' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:13.660' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:13.893' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:14.033' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:14.313' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:14.463' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:14.810' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:15.060' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:15.280' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:15.487' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:15.653' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:15.837' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:15.993' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:16.230' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:18.113' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:18.480' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:18.817' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:25.543' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:26.183' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:26.473' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:26.593' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:26.957' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:27.220' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:27.330' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:27.757' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:27.977' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:28.193' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:28.393' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:28.727' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:29.020' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:29.170' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:29.323' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/song/31', CAST(N'2022-03-22T18:11:29.810' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:34.623' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:35.280' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:35.553' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:35.673' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:36.030' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:36.407' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/1', CAST(N'2022-03-22T18:11:36.757' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:38.113' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:38.717' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:38.937' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:39.057' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:39.280' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:39.513' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:39.623' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:39.890' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:40.003' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:40.243' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:40.440' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:40.670' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:40.807' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:41.037' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:41.217' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:41.373' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/playlist/5', CAST(N'2022-03-22T18:11:41.590' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'canhdm', N'http://localhost:5026/logout', CAST(N'2022-03-22T18:11:43.260' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/', CAST(N'2022-03-22T18:11:43.453' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (NULL, N'http://localhost:5026/login', CAST(N'2022-03-22T18:11:44.373' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/', CAST(N'2022-03-22T18:11:50.280' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:54.853' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:55.870' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:56.097' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:56.213' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:56.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/23', CAST(N'2022-03-22T18:11:56.827' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:58.263' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:58.617' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:58.883' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:59.037' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:59.260' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:59.390' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:11:59.790' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:00.183' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:00.453' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:00.627' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:01.020' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:01.330' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:01.563' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:01.860' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:01.997' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:02.380' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/27', CAST(N'2022-03-22T18:12:02.747' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:07.747' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:08.487' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:08.713' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:08.953' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:09.133' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:09.297' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:09.503' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:09.667' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:09.883' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:10.110' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:10.267' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:10.490' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:10.630' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:10.883' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:11.013' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:11.360' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:11.633' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:11.767' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:12.027' AS DateTime))
GO
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:12.167' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:12.843' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:13.237' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/song/34', CAST(N'2022-03-22T18:12:13.593' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:17.770' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:18.640' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:19.017' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:19.270' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:19.423' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:19.603' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:19.770' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:20.123' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:20.380' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:20.583' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:20.747' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:20.970' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:21.183' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:21.440' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:21.607' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:21.867' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:22.017' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:22.197' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:22.457' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:22.983' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:24.270' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:24.547' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:24.700' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:24.943' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:25.097' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:25.350' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:25.483' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:26.100' AS DateTime))
INSERT [dbo].[WebTracking] ([Username], [ContextURL], [Date]) VALUES (N'quynhln', N'http://localhost:5026/playlist/2', CAST(N'2022-03-22T18:12:26.657' AS DateTime))
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Account] ADD  DEFAULT ('/img/defaultAvatar.jpg') FOR [Avatar]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Feedback] ADD  DEFAULT ((0)) FOR [Priority]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT ('/img/defaultPlaylistThumbnail.jpg') FOR [Thumbnail]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Playlist] ADD  DEFAULT ((1)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[PlaylistSong] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[PlaylistTracking] ADD  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Thumbnail]  DEFAULT ('/img/defaultSongThumbnail.jpg') FOR [Thumbnail]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Cover]  DEFAULT ('/img/defaultSongCover.jpg') FOR [Cover]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[Song] ADD  CONSTRAINT [DF_Song1_IsPublic]  DEFAULT ((1)) FOR [IsPublic]
GO
ALTER TABLE [dbo].[Song] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[SongTracking] ADD  CONSTRAINT [DF_SongTracking1_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[WebTracking] ADD  DEFAULT (getdate()) FOR [Date]
GO
USE [master]
GO
ALTER DATABASE [MUZIC] SET  READ_WRITE 
GO

var tabContent = document.querySelectorAll(".tab-content");
var checkRobot = document.querySelector(".check-robot");
var checkAddSong = document.querySelectorAll(".check-addsong");
var checkBoxRobot = document.querySelector(".fa-square");
var tabInner = document.querySelectorAll(".tab-inner");
var iconPublic = document.querySelectorAll(".icon-public");
var iconDelete = document.querySelectorAll(".icon-delete");
var iconDeleteMyfavoritePlaylist = document.querySelectorAll(".icon-delete2");
var iconDeleteMyPlaylist = document.querySelectorAll(".icon-delete3");
var tabMysongList = document.querySelector(".tab-mysong-list");
var exitPopup = document.querySelectorAll(".exit-button");
var exitPopupMyPlaylist = document.querySelector(".exit-button-mpl");
var editPopup = document.querySelectorAll(".edit-button");
var editPopupMyPlaylist = document.querySelectorAll(".edit-button-mpl");
var uploadSongButton = document.querySelector(".mysong-upload");
var iconMoreSongPlaylist = document.querySelectorAll(".icon-playlist-more");
var iconMoreSongPlaylist2 = document.querySelectorAll(".icon-playlist-more2");
var iconMinus = document.querySelectorAll(".fa-minus");

window.onload = function () {
  iconMinus.forEach(function (icon) {
    icon.addEventListener("click", () => {
      icon.parentNode.remove();
    });
  });

  iconMoreSongPlaylist.forEach(function (icon) {
    icon.addEventListener("click", () => {
      var iconChild = icon.firstElementChild;
      if (iconChild.classList.contains("fa-angle-right")) {
        icon.parentNode.parentNode.parentNode.childNodes[1].nextElementSibling.classList.add(
          "active-playlist-song"
        );
        iconChild.classList.remove("fa-angle-right");
        iconChild.classList.add("fa-angle-down");
      } else if (iconChild.classList.contains("fa-angle-down")) {
        icon.parentNode.parentNode.parentNode.childNodes[1].nextElementSibling.classList.remove(
          "active-playlist-song"
        );
        iconChild.classList.remove("fa-angle-down");
        iconChild.classList.add("fa-angle-right");
      }
    });
  });

  iconMoreSongPlaylist2.forEach(function (icon) {
    icon.addEventListener("click", () => {
      var iconChild = icon.firstElementChild;
      if (iconChild.classList.contains("fa-angle-right")) {
        icon.parentNode.parentNode.parentNode.childNodes[1].nextElementSibling.classList.add(
          "active-playlist-song"
        );
        iconChild.classList.remove("fa-angle-right");
        iconChild.classList.add("fa-angle-down");
      } else if (iconChild.classList.contains("fa-angle-down")) {
        icon.parentNode.parentNode.parentNode.childNodes[1].nextElementSibling.classList.remove(
          "active-playlist-song"
        );
        iconChild.classList.remove("fa-angle-down");
        iconChild.classList.add("fa-angle-right");
      }
    });
  });

  if (checkRobot) {
    checkRobot.addEventListener("click", function () {
      if (checkRobot.classList.contains("fa-square")) {
        checkRobot.classList.remove("fa-square");
        checkRobot.classList.add("fa-check");
        checkRobot.style.color = "var(--green)";
        checkRobot.parentNode.style.border = "1px solid var(--green)";
      } else if (checkRobot.classList.contains("fa-check")) {
        checkRobot.classList.remove("fa-check");
        checkRobot.classList.add("fa-square");
        checkRobot.style.color = "white";
        checkRobot.parentNode.style.border = "none";
      }
    });
  }
  checkAddSong.forEach(function (e) {
    e.addEventListener("click", () => {
      if (e.classList.contains("fa-square")) {
        e.classList.remove("fa-square");
        e.classList.add("fa-check");
        e.style.color = "var(--green)";
      } else if (e.classList.contains("fa-check")) {
        e.classList.remove("fa-check");
        e.classList.add("fa-square");
        e.style.color = "white";
      }
    });
  });

  uploadSongButton.addEventListener("click", function () {
    document
      .querySelector(".tab-mysong-popup-upload-container")
      .classList.add("active-popup");
  });

  editPopup.forEach(function (icon) {
    icon.addEventListener("click", () => {
      document
        .querySelector(".tab-mysong-popup-edit-container")
        .classList.add("active-popup");
    });
  });
  editPopupMyPlaylist.forEach(function (icon) {
    icon.addEventListener("click", () => {
      document
        .querySelector(".tab-myplaylist-popup-edit-container")
        .classList.add("active-popup");
    });
  });
  exitPopup.forEach(function (icon) {
    icon.addEventListener("click", () => {
      if (
        document
          .querySelector(".tab-mysong-popup-upload-container")
          .classList.contains("active-popup")
      ) {
        document
          .querySelector(".tab-mysong-popup-upload-container")
          .classList.remove("active-popup");
      }
      if (
        document
          .querySelector(".tab-mysong-popup-edit-container")
          .classList.contains("active-popup")
      ) {
        document
          .querySelector(".tab-mysong-popup-edit-container")
          .classList.remove("active-popup");
      }
    });
  });

  exitPopupMyPlaylist.addEventListener("click", () => {
    if (
      document
        .querySelector(".tab-myplaylist-popup-edit-container")
        .classList.contains("active-popup")
    ) {
      document
        .querySelector(".tab-myplaylist-popup-edit-container")
        .classList.remove("active-popup");
    }
    if (
      document
        .querySelector(".tab-myplaylist-popup-edit-container")
        .classList.contains("active-popup")
    ) {
      document
        .querySelector(".tab-myplaylist-popup-edit-container")
        .classList.remove("active-popup");
    }
  });

  iconDelete.forEach(function (icon) {
    icon.addEventListener("click", () => {
      icon.parentNode.parentNode.remove();
    });
  });

  iconDeleteMyfavoritePlaylist.forEach(function (icon) {
    icon.addEventListener("click", () => {
      icon.parentNode.parentNode.parentNode.remove();
    });
  });
  iconDeleteMyPlaylist.forEach(function (icon) {
    icon.addEventListener("click", () => {
      icon.parentNode.parentNode.parentNode.remove();
    });
  });

  iconPublic.forEach(function (icon) {
    icon.addEventListener("click", () => {
      var iconChild = icon.firstElementChild;
      if (iconChild.classList.contains("fa-lock")) {
        icon.classList.add("icon-public-active");
        iconChild.classList.remove("fa-lock");
        iconChild.classList.add("fa-unlock");
      } else if (iconChild.classList.contains("fa-unlock")) {
        icon.classList.remove("icon-public-active");
        iconChild.classList.remove("fa-unlock");
        iconChild.classList.add("fa-lock");
      }
    });
  });
};
tabContent.forEach(function (tab) {
  tab.addEventListener("click", () => {
    removeActiveTab(tabContent);
    tab.classList.add("tab-active");
    displayTabInner(tabInner, tab);
  });
});

function removeActiveTab(tabs) {
  tabs.forEach(function (e) {
    e.classList.remove("tab-active");
  });
}
function removeActiveTabInner(tabs) {
  tabs.forEach(function (e) {
    e.classList.remove("tab-inner-active");
  });
}

function displayTabInner(tabs, tab) {
  if (tab.classList.contains("mysong")) {
    removeActiveTabInner(tabs);
    document
      .querySelector(".tab-mysong-inner")
      .classList.add("tab-inner-active");
  } else if (tab.classList.contains("myplaylist")) {
    removeActiveTabInner(tabs);
    document
      .querySelector(".tab-myplaylist-inner")
      .classList.add("tab-inner-active");
  } else if (tab.classList.contains("setting")) {
    removeActiveTabInner(tabs);
    document
      .querySelector(".tab-setting-inner")
      .classList.add("tab-inner-active");
  } else if (tab.classList.contains("myfavorite")) {
    removeActiveTabInner(tabs);
    document
      .querySelector(".tab-myfavorite-inner")
      .classList.add("tab-inner-active");
  }
}
